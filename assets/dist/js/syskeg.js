var table;
$(document).ready(function() {
	    table = $('#daftar_kegiatan').DataTable( {
        'searching': true,
        'paging':   true,
        'ordering': false,
        'info':     false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': 'daftar-kegiatan/tabel-view',
            'type': 'POST',

        },
        "columnDefs": [
                        { className: "kodkgt", "targets": [ 1 ] }
                      ],
        'columnDefs': [
                        {
                            'targets': 1,
                            'createdCell':  function (td, cellData, rowData, row, col) {
                                $(td).attr('cr_code', 'cell-' + cellData); 
                            }
                        },
                        {
                            'targets': 2,
                            'createdCell':  function (td, cellData, rowData, row, col) {
                                $(td).attr('cr_uraian', 'cell'); 
                            }
                        }
        ],
        
         responsive: true,
        'language': {
    'sEmptyTable':     'No data available in table',
    'sInfo':           'Showing _START_ to _END_ of _TOTAL_ entries',
    'sInfoEmpty':      'Showing 0 to 0 of 0 entries',
    'sInfoFiltered':   '(filtered from _MAX_ total entries)',
    'sInfoPostFix':    '',
    'sInfoThousands':  ',',
    'sLengthMenu':     'Show _MENU_ entries',
    'sLoadingRecords': 'Loading...',
    'sProcessing':     'Processing...',
    'sSearch':         'Search:',
    'sZeroRecords':    'No matching records found',
    'oPaginate': {
        'sFirst':    'First',
        'sLast':     'Last',
        'sNext':     'Next',
        'sPrevious': 'Previous'
    },
    'oAria': {
        'sSortAscending':  ': activate to sort column ascending',
        'sSortDescending': ': activate to sort column descending'
    }
}
    } );
 return set_tok(csfrData);
});
function reload_table(){
      table.ajax.reload(null,false);
}
function resetForm(){
    $('#modaddkegiatan').trigger('reset');
}
$('.autonumber').autoNumeric('init', { currencySymbol : 'Rp' });
$('#addata').click(function(){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    resetForm();
    $('#addmodal').find('form')[0].reset();
    $('#modaddkegiatan').formValidation('resetForm', true);
    $('.autonumber').autoNumeric('init', { currencySymbol : 'Rp' });
    $('#addmodal').modal('show').on('shown.bs.modal');
    $.unblockUI();
});
$('input[name="jmhanggaran"]').change(function () {
    var jmhanggaran = $(this).autoNumeric('get');
     $('input[name="jmhanggaranhd"]').val(jmhanggaran);
});
        /*$('input[name="ssanggaran"]').change(function () {
            var ssanggaran = $(this).autoNumeric('get');
             $('input[name="jmhanggaranhd"]').val(ssanggaranhd);
        });*/
        $(document).ready(function() {
        $('#modaddkegiatan').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            valid: '',
            invalid: '',
            validating: ''
        },
        row: {
            valid: 'field-success',
            invalid: 'field-error'
        },
        locale: 'id_ID',
        fields: {
            cdkegiatan: {
                validators: {
                    notEmpty: {
                        
                    },
                    numeric:{

                    },
                    callback: {
                            callback: function(value, validator, $field) {
                                var digitsCount = value.search(/[0-9]/);
                                if (value.length !== 11) {
                                    return {
                                        valid: false,
                                        message: " Harus 11 karakter angka."
                                    }
                                }
                                return true;
                            }
                        },
                    remote: {
                        url: 'daftar-kegiatan/data-auths',
                        type: 'POST',
                        delay: 3000
                    }
                }
            },
            ketkegiatan: {
                validators: {
                    notEmpty: {
                        
                    }
                }
            }

        }
    })    
       .on('success.form.fv', function(e) {
        e.preventDefault();
        $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
        $.ajax({
            type    : 'POST',
            url     : 'daftar-kegiatan/data-save',
            data    : $('#modaddkegiatan').serialize(),
            dataType: 'json',
            async: true,
            success : function(response){
                 if(response.msg == 'success'){
                    reload_table();
                    $('#addmodal').modal('hide').on('shown.bs.modal');
                    $.unblockUI();
                    toastr.success('Add Data has been success !');
                }else{
                    $('#addmodal').modal('hide').on('shown.bs.modal');
                    $.unblockUI();
                    toastr.error('Add Data has been Fail !');                 
                }
            }
        });
    });
});
function confdel(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    $('#delmodal').modal('hide').on('shown.bs.modal');
    $.ajax({
        type    : 'POST',
        url     : 'daftar-kegiatan/data-delete',
        data    : {take:ot},
        dataType: 'json',
        async: false,
        success : function(response){
            if(response.msg == true){
                reload_table();
                $.unblockUI();
                toastr.success('Delete has been success !');
            }else{
                $.unblockUI();
                toastr.error('Delete has been Fail !');                 
            }
        }
    });
};
function eddat(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    $('#editmodal').find('form')[0].reset();
    $('#modeditkegiatan').formValidation('resetForm', true);
        $.ajax({
            type    : 'POST',
            url     : 'daftar-kegiatan/data-edit',
            data    : {take:ot},
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == false){
                    $.unblockUI();
                    toastr.error('List Not available !');
                }else{
                    var $rowedit = $('.deldata'+ot).closest('tr');
                    var $textedit = $rowedit.find('[cr_code]').text();
                    var $texturaianedit = $rowedit.find('[cr_uraian]').text();
                    $('#editcdkegiatan').val($textedit);
                    $('#editketkegiatan').val($texturaianedit);

                    $('#editjmhanggaran').autoNumeric('set', response.a);

                    $('#edittotpengeluaran').autoNumeric('set', response.b);
                    //$('#editjmhanggaran').val(response.a);
                    $('#editssanggaran').autoNumeric('set', response.c);
                    //$('#editssanggaran').val(response.b);

                    $('#edittglkgtn').val(response.d+' -- Last Update');
                    $('#editmodal').modal('show').on('shown.bs.modal');
                    $.unblockUI();
                }
                    
            }
        });
}
function deldat(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    var $row = $('.deldata'+ot).closest('tr');
    var $text = $row.find('[cr_code]').text();
    var $texturaian = $row.find('[cr_uraian]').text();
    $('#text').empty();
    $('#text').append('<span style="font-size:5rem;color:#f0ad4e;"><i class="fa fa-fw fa-trash-o" style="cursor: unset !important"></i></span><br/><span style="font-size: 1.75rem;color: #818a91 !important;">'+$text+'</span><br/><span style="font-size: 1.25rem;color: #818a91 !important;">'+$texturaian+'</span>');
    $('div.modal-body #confdel').attr('onclick','confdel("'+ot+'")');
    $('#delmodal').modal('show').on('shown.bs.modal');
    $.unblockUI();
};
 $('input[name="editjmhanggaran"]').change(function () {
    var editjmhanggaran = $(this).autoNumeric('get');
     $('input[name="editjmhanggaranhd"]').val(editjmhanggaran);
});
$(document).ready(function() {
        $('#modeditkegiatan').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        row: {
            valid: 'field-success',
            invalid: 'field-error'
        },
        locale: 'id_ID',
        fields: {
            editcdkegiatan: {
                validators: {
                    remote: {
                        url: 'daftar-kegiatan/data-auths-edit',
                        type: 'POST',
                        delay: 3000
                    },
                    notEmpty: {
                        
                    },
                    numeric:{

                    },
                    callback: {
                            callback: function(value, validator, $field) {
                                var digitsCount = value.search(/[0-9]/);
                                if (value.length !== 11) {
                                    return {
                                        valid: false,
                                        message: " Harus 11 karakter angka."
                                    }
                                }
                                return true;
                            }
                        }
                }
            }

        }
    })

        .on('err.validator.fv', function(e, data) {
            if (data.field === 'mail') {
                data.element
                    .data('fv.messages')
                    .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                    .filter('[data-fv-validator="' + data.validator + '"]').show();
            }
        })
    
       .on('success.form.fv', function(e) {
        e.preventDefault();
        $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
        $.ajax({
            type    : 'POST',
            url     : 'daftar-kegiatan/data-update',
            data    : $('#modeditkegiatan').serialize(),
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == 'success'){
                    reload_table();
                    $('#editmodal').modal('hide').on('shown.bs.modal');
                    $.unblockUI();
                    toastr.success('Edit has been success !');
                }else{
                    $('#editmodal').modal('hide').on('shown.bs.modal');
                    $.unblockUI();
                    toastr.error('Edit has been Fail !');                 
                }
            }
        });
    });
});
/*
$(document).ready(function() {
        $('#modaddkegiatan').formValidation({
        framework: 'bootstrap',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        row: {
            valid: 'field-success',
            invalid: 'field-error'
        },
        locale: 'id_ID',
        fields: {
            cdkegiatan: {
                validators: {
                    notEmpty: {
                        
                    },
                    numeric:{

                    },
                    callback: {
                            callback: function(value, validator, $field) {
                                var digitsCount = value.search(/[0-9]/);
                                if (value.length !== 11) {
                                    return {
                                        valid: false,
                                        message: " Harus 11 karakter angka."
                                    }
                                }
                                return true;
                            }
                        }
                }
            },
            ketkegiatan: {
                validators: {
                    notEmpty: {
                        
                    }
                }
            }

        }
    })

        .on('err.validator.fv', function(e, data) {
            if (data.field === 'mail') {
                data.element
                    .data('fv.messages')
                    .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                    .filter('[data-fv-validator="' + data.validator + '"]').show();
            }
        })
    
       .on('success.form.fv', function(e) {
        e.preventDefault();
        $.ajax({
            type    : 'POST',
            url     : 'auth',
            data    : $('#login_form').serialize(),
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == 'success'){
                    window.location.href = 'dashboard.html';
                }else if(response.msg == 'error'){
                    $('[name="dis-token"]').val(response.disperindag);
                    toastr.error('username atau password yang anda masukkan salah !');                 
                }else if(response.msg == 'info'){
                    $('[name="dis-token"]').val(response.disperindag);
                    toastr.info('Verifikasi telah dikirim ke email, mohon di periksa kembali !');
                }else{
                    $('[name="dis-token"]').val(response.disperindag);
                    toastr.warning('Akun anda dibekukan, silahkan hubungi Admin !');
                }
            }
        });
    });
       });*/