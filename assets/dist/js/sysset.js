var table;
$(document).ready(function() {
    table = $('#data_settings').DataTable( {
        'searching': true,
        'paging':   true,
        'ordering': false,
        'info':     false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': 'settings-view',
            'type': 'POST',

        },
        'columnDefs': [
                        {
                            'targets': 1,
                            'createdCell':  function (td, cellData, rowData, row, col) {
                                $(td).attr('cr_nmuser', 'cell'); 
                            }
                        },
                        {
                            'targets': 2,
                            'createdCell':  function (td, cellData, rowData, row, col) {
                                $(td).attr('cr_email', 'cell'); 
                            }
                        }
        ],        
         responsive: true,
        'language': {
    'sEmptyTable':     'No data available in table',
    'sInfo':           'Showing _START_ to _END_ of _TOTAL_ entries',
    'sInfoEmpty':      'Showing 0 to 0 of 0 entries',
    'sInfoFiltered':   '(filtered from _MAX_ total entries)',
    'sInfoPostFix':    '',
    'sInfoThousands':  ',',
    'sLengthMenu':     'Show _MENU_ entries',
    'sLoadingRecords': 'Loading...',
    'sProcessing':     'Processing...',
    'sSearch':         'Search:',
    'sZeroRecords':    'No matching records found',
    'oPaginate': {
        'sFirst':    'First',
        'sLast':     'Last',
        'sNext':     'Next',
        'sPrevious': 'Previous'
    },
    'oAria': {
        'sSortAscending':  ': activate to sort column ascending',
        'sSortDescending': ': activate to sort column descending'
    }
}
    } );
 return set_tok(csfrData);
});
function reload_table(){
      table.ajax.reload(null,false);
}
function resetForm(){
    $(".fclass").val('');
    $(".fclass").selectpicker("refresh");
}
$('#addata').click(function(){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    resetForm('#modadduser');
    $('#addmodal').find('form')[0].reset();
    $('#modadduser').formValidation('resetForm', true);
    $('#addmodal').modal('show').on('shown.bs.modal');
    $.unblockUI();
});
$(document).ready(function() {
        $('#modadduser')
        .find('.fclass')
            .selectpicker()
            .change(function(e) {
                /* Revalidate the language when it is changed */
                $('#modadduser').formValidation('revalidateField', 'level');
            })
            .end()
        .formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        row: {
            valid: 'field-success',
            invalid: 'field-error'
        },
        locale: 'id_ID',
        fields: {
            nama: {
                validators: {
                    notEmpty: {
                        
                    }
                }
            },
            email: {
                validators: {
                    remote: {
                        url: url_link+'check-user',
                        type: 'POST',
                        delay: 3000
                    },
                    notEmpty: {
                        
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        
                    },
                    emailAddress: {
                       
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        
                    }                    
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        
                    },
                    different: {
                        field: 'email'
                    },
                    stringLength: {
                        min: 6,
                        max: 30
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/
                    }
                }
            },
            repassword: {
                validators: {
                    notEmpty: {
                        
                    },
                    identical: {
                        field: 'password'
                    }
                }
            }

        }
    })
        .on('err.validator.fv', function(e, data) {
            if (data.field === 'email') {
                data.element
                    .data('fv.messages')
                    .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                    .filter('[data-fv-validator="' + data.validator + '"]').show();
            }
        })
    
       .on('success.form.fv', function(e) {
        e.preventDefault();
        $.ajax({
            type    : 'POST',
            url     : url_link+'add-user',
            data    : $('#modadduser').serialize(),
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == 'success'){
                    $('#addmodal').modal('hide').on('shown.bs.modal');
                    toastr.success('Add Data has been success !');
                    reload_table();
                }else{
                    $('#addmodal').modal('hide').on('shown.bs.modal');
                    toastr.error('Add Data has been Fail !');                 
                }
            }
        });
    });
});
function deldat(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    var $row = $('.deldata'+ot).closest('tr');
    var $text = $row.find('[cr_nmuser]').text();
    var $textemail = $row.find('[cr_email]').text();
    $('#text').empty();
    $('#text').append('<span style="font-size:5rem;color:#f0ad4e;"><i class="fa fa-fw fa-trash-o" style="cursor: unset !important"></i></span><br/><span style="font-size: 1.75rem;color: #818a91 !important;">'+$text+'</span><br/><span style="font-size: 1.25rem;color: #818a91 !important;">'+$textemail+'</span>');
    $('div.modal-body #confdel').attr('onclick','confdel("'+ot+'")');
    $('#deltrans').modal('show').on('shown.bs.modal');
    $.unblockUI();
};
function confdel(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    $('#deltrans').modal('hide').on('shown.bs.modal');
    $.ajax({
        type    : 'POST',
        url     : 'delete-user',
        data    : {take:ot},
        dataType: 'json',
        async: false,
        success : function(response){
            if(response.msg == true){
                reload_table();
                $.unblockUI();
                toastr.success('Delete has been success !');
            }else{
                $.unblockUI();
                toastr.error('Delete has been Fail !');                 
            }
        }
    });
};
$(document).ready(function() {
        $('#editprof').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        row: {
            valid: 'field-success',
            invalid: 'field-error'
        },
        locale: 'id_ID',
        fields: {
            nama: {
                validators: {
                    notEmpty: {
                        
                    }
                }
            }
        }
    })
        .on('err.validator.fv', function(e, data) {
            if (data.field === 'nama') {
                data.element
                    .data('fv.messages')
                    .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                    .filter('[data-fv-validator="' + data.validator + '"]').show();
            }
        })
    
       .on('success.form.fv', function(e) {
        e.preventDefault();
        $.ajax({
            type    : 'POST',
            url     : url_link+'profile-update',
            data    : $('#editprof').serialize(),
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == 'success'){
                    $('#addmodal').modal('hide').on('shown.bs.modal');
                    toastr.success('Edit Data has been success !');
                    reload_table();
                }else{
                    $('#addmodal').modal('hide').on('shown.bs.modal');
                    toastr.error('Edit Data has been Fail !');                 
                }
            }
        });
    });
});
$(document).ready(function() {
        $('#editpass').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        row: {
            valid: 'field-success',
            invalid: 'field-error'
        },
        locale: 'id_ID',
        fields: {
            cpass: {
                validators: {
                    notEmpty: {
                        
                    }
                }
            },
            npass: {
                validators: {
                    notEmpty: {
                        
                    },
                    different: {
                        field: 'cpass'
                    },
                    stringLength: {
                        min: 6,
                        max: 30
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/
                    }
                }
            },
            ncpass: {
                validators: {
                    notEmpty: {
                        
                    },
                    identical: {
                        field: 'npass'
                    }
                }
            }

        }
    })
        .on('err.validator.fv', function(e, data) {
            if (data.field === 'cpass') {
                data.element
                    .data('fv.messages')
                    .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                    .filter('[data-fv-validator="' + data.validator + '"]').show();
            }
        })
    
       .on('success.form.fv', function(e) {
        e.preventDefault();
        $.ajax({
            type    : 'POST',
            url     : url_link+'profile-update-password',
            data    : $('#editpass').serialize(),
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == 'success'){
                    $('#addmodal').modal('hide').on('shown.bs.modal');
                    toastr.success('Edit Data has been success !');
                    reload_table();
                }else{
                    $('#addmodal').modal('hide').on('shown.bs.modal');
                    toastr.error('Edit Data has been Fail !');                 
                }
            }
        });
    });
});