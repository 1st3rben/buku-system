var table;
$(document).ready(function() {
    table = $('#daftar_belanja').DataTable( {
        'searching': true,
        'paging':   true,
        'ordering': false,
        'info':     false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': url_link+'daftar-kegiatan/daftar-belanja/tabel-view',
            'type': 'POST'
        },
        'columnDefs': [
                        {
                            'targets': 1,
                            'createdCell':  function (td, cellData, rowData, row, col) {
                                $(td).attr('cr_code', 'cell-' + cellData); 
                            }
                        },
                        {
                            'targets': 2,
                            'createdCell':  function (td, cellData, rowData, row, col) {
                                $(td).attr('cr_keter', 'cell'); 
                            }
                        }
        ],
        
         responsive: true,
        'language': {
        'sEmptyTable':     'No data available in table',
        'sInfo':           'Showing _START_ to _END_ of _TOTAL_ entries',
        'sInfoEmpty':      'Showing 0 to 0 of 0 entries',
        'sInfoFiltered':   '(filtered from _MAX_ total entries)',
        'sInfoPostFix':    '',
        'sInfoThousands':  ',',
        'sLengthMenu':     'Show _MENU_ entries',
        'sLoadingRecords': 'Loading...',
        'sProcessing':     'Processing...',
        'sSearch':         'Search:',
        'sZeroRecords':    'No matching records found',
        'oPaginate': {
            'sFirst':    'First',
            'sLast':     'Last',
            'sNext':     'Next',
            'sPrevious': 'Previous'
        },
        'oAria': {
            'sSortAscending':  ': activate to sort column ascending',
            'sSortDescending': ': activate to sort column descending'
        }
    }
    } );
 return set_tok(csfrData);
});
function reload_table(){
      table.ajax.reload(null,false);
}

$('.autonumber').autoNumeric('init', { currencySymbol : 'Rp' });
$('#addata').click(function(){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    resetForm('#modaddbelanja');
    $('#cdkegiatan').val(null).trigger('change');
    $('#addmodal').find('form')[0].reset();
    $('#modaddbelanja').formValidation('resetForm', true);
    $('.autonumber').autoNumeric('init', { currencySymbol : 'Rp' });
    $('#addmodal').modal('show').on('shown.bs.modal');
    $.unblockUI();
});
        $('input[name="jmhanggaran"]').change(function () {
            var jmhanggaran = $(this).autoNumeric('get');
             $('input[name="jmhanggaranhd"]').val(jmhanggaran);
        });
$(document).ready(function() {
        $('#modaddbelanja').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        row: {
            valid: 'field-success',
            invalid: 'field-error'
        },
        locale: 'id_ID',
        fields: {
            cdbelanja: {
                validators: {
                    remote: {
                        url: url_link+'daftar-kegiatan/belanja-data-auths',
                        type: 'POST',
                        delay: 3000
                    },
                    notEmpty: {
                        
                    },
                    numeric:{

                    },
                    callback: {
                            callback: function(value, validator, $field) {
                                var digitsCount = value.search(/[0-9]/);
                                if (value.length !== 8) {
                                    return {
                                        valid: false,
                                        message: " Harus 8 karakter angka."
                                    }
                                }
                                return true;
                            }
                        }
                }
            },
            ketbelanja: {
                validators: {
                    notEmpty: {
                        
                    }
                }
            }

        }
    })
        .on('err.validator.fv', function(e, data) {
            if (data.field === 'mail') {
                data.element
                    .data('fv.messages')
                    .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                    .filter('[data-fv-validator="' + data.validator + '"]').show();
            }
        })
    
       .on('success.form.fv', function(e) {
        e.preventDefault();
        $.ajax({
            type    : 'POST',
            url     : url_link+'daftar-kegiatan/belanja-data-save',
            data    : $('#modaddbelanja').serialize(),
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == 'success'){
                    $('#addmodal').modal('hide').on('shown.bs.modal');
                    toastr.success('Add Data has been success !');
                    reload_table();
                }else{
                    $('#addmodal').modal('hide').on('shown.bs.modal');
                    toastr.error('Add Data has been Fail !');                 
                }
            }
        });
    });
});
function deldat(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    var $row = $('.deldata'+ot).closest('tr');
    var $text = $row.find('[cr_code]').text();
    var $texturaian = $row.find('[cr_keter]').text();
    $('#text').empty();
    $('#text').append('<span style="font-size:5rem;color:#f0ad4e;"><i class="fa fa-fw fa-trash-o" style="cursor: unset !important"></i></span><br/><span style="font-size: 1.75rem;color: #818a91 !important;">'+$text+'</span><br/><span style="font-size: 1.25rem;color: #818a91 !important;">'+$texturaian+'</span>');
    $('div.modal-body #confdel').attr('onclick','confdel("'+ot+'")');
    $('#delmodal').modal('show').on('shown.bs.modal');
    $.unblockUI();
};
function confdel(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    $('#delmodal').modal('hide').on('shown.bs.modal');   
    $.ajax({
        type    : 'POST',
        url     : url_link+'daftar-kegiatan/belanja-data-delete',
        data    : {take:ot},
        dataType: 'json',
        async: false,
        success : function(response){
            if(response.msg == true){
                reload_table();
                $.unblockUI();
                toastr.success('Delete has been success !');
            }else{
                $.unblockUI();
                toastr.error('Delete has been Fail !');                 
            }
        }
    });
};
 $('input[name="editjmhanggaran"]').change(function () {
    var editjmhanggaran = $(this).autoNumeric('get');
     $('input[name="editjmhanggaranhd"]').val(editjmhanggaran);
});
function eddat(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    $('#editmodal').find('form')[0].reset();
    resetForm('#modeditbelanja');
    $('#modeditbelanja').formValidation('resetForm', true);
        $.ajax({
            type    : 'POST',
            url     : url_link+'daftar-kegiatan/belanja-data-edit',
            data    : {take:ot},
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == false){
                    $.unblockUI();
                    toastr.error('List Not available !');
                }else{
                    var $rowedit = $('.deldata'+ot).closest('tr');
                    var $textedit = $rowedit.find('[cr_code]').text();
                    var $texturaianedit = $rowedit.find('[cr_keter]').text();
                    $('#editcdbelanja').val($textedit);
                    $('#editketbelanja').val($texturaianedit);

                    $('#editjmhanggaran').autoNumeric('set', response.a);
                    $('input[name="editjmhanggaranhd"]').val(response.a);
                    $('#editcdkegiatan').val(response.e).trigger('change');
                    $('#edittransaksi').autoNumeric('set', response.b);
                    $('input[name="edittransaksihd"]').val(response.b);
                    //$('#editjmhanggaran').val(response.a);
                    $('#editlastmodal').autoNumeric('set', response.c);
                    $('input[name="editlastmodalhd"]').val(response.c);
                    //$('#editssanggaran').val(response.b);

                    $('#edittglkgtn').val(response.d+' -- Last Update');
                    $('#editmodal').modal('show').on('shown.bs.modal');
                    $.unblockUI();
                }
                    
            }
        });
}
$(document).ready(function() {
    $('#editjmhanggaran').on('change', function() {
        // Set value for fields
        var anggarandata3=0;
        var anggarandata0 = 0;
        if($('input[name="editjmhanggaranhd"]').val()>0){
            var anggarandata0 = $('input[name="editjmhanggaranhd"]').val();
        }
        var anggarandata1 = $('input[name="edittransaksihd"]').val();
        var anggarandata2 = $('input[name="editlastmodalhd"]').val();
        var anggarandata3=parseInt(anggarandata2)+(parseInt(anggarandata0)-parseInt(anggarandata1));
        $('input[name="editlastmodalhd"]').val(anggarandata3);
        $('input[name="editlastmodal"]').autoNumeric('set', anggarandata3);
                                

        // Revalidate the fields
        $('#modeditbelanja').formValidation('revalidateField', 'editjmhanggaranhd');
    });

        $('#modeditbelanja').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        row: {
            valid: 'field-success',
            invalid: 'field-error'
        },
        locale: 'id_ID',
        fields: {
            editcdbelanja: {
                validators: {
                    remote: {
                        url: url_link+'daftar-kegiatan/belanja-data-auths-edit',
                        type: 'POST',
                        delay: 3000
                    },
                    notEmpty: {
                        
                    },
                    numeric:{

                    },
                    callback: {
                            callback: function(value, validator, $field) {
                                var digitsCount = value.search(/[0-9]/);
                                if (value.length !== 8) {
                                    return {
                                        valid: false,
                                        message: " Harus 8 karakter angka."
                                    }
                                }
                                return true;
                            }
                        }
                }
            },
            ketbelanja: {
                validators: {
                    notEmpty: {
                        
                    }
                }
            },
            editjmhanggaranhd: {
                validators: {
                    notEmpty: {
                        
                    },
                    callback: {
                            message: 'Jumlah Terlampau Kecil !!!!',
                            callback: function(value, validator, $field) {
                                var anggarandata0 = 0;
                                if($('input[name="editjmhanggaranhd"]').val()>0){
                                    var anggarandata0 = $('input[name="editjmhanggaranhd"]').val();
                                }
                                var anggarandata1 = $('input[name="edittransaksihd"]').val();
                                var anggarandata2 = $('input[name="editlastmodalhd"]').val();
                                var anggarandata3=0;
                                var anggarandata3=parseInt(anggarandata2)+(parseInt(anggarandata0)-parseInt(anggarandata1));
                                if(anggarandata3 < 0){
                                    return {
                                        valid: false
                                    }
                                }
                                return true;
                            }
                        }
                }
            }

        }
    })

        .on('err.validator.fv', function(e, data) {
            if (data.field === 'editjmhanggaranhd') {
                data.element
                    .data('fv.messages')
                    .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                    .filter('[data-fv-validator="' + data.validator + '"]').show();
            }
        })
    
       .on('success.form.fv', function(e) {
        e.preventDefault();
        $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
        $.ajax({
            type    : 'POST',
            url     : url_link+'daftar-kegiatan/belanja-data-update',
            data    : $('#modeditbelanja').serialize(),
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == 'success'){
                    reload_table();
                    $('#editmodal').modal('hide').on('shown.bs.modal');
                    $.unblockUI();
                    toastr.success('Edit has been success !');
                }else{
                    $('#editmodal').modal('hide').on('shown.bs.modal');
                    $.unblockUI();
                    toastr.error('Edit has been Fail !');                 
                }
            }
        });
    });
});
function adtrans(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    $('#transmodal').find('form')[0].reset();
    resetForm('#modtransbelanja');
    $('.fungsitrans').empty();
    $('#modtransbelanja').formValidation('resetForm', true);
        $.ajax({
            type    : 'POST',
            url     : url_link+'daftar-kegiatan/belanja-data-buy',
            data    : {take:ot},
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == false){
                    $.unblockUI();
                    toastr.error('List Not available !');
                }else{
                    var $rowtrans = $('.deldata'+ot).closest('tr');
                    var $kodehidden = $rowtrans.find('[cr_code]').text();
                    var $fungsibelanja = $rowtrans.find('[cr_keter]').text();
                    $('.cdbelanjatrans').val($kodehidden);
                    $('.fungsitrans').text($fungsibelanja);
                    $('#jmhhgtrans').autoNumeric('set', response.b);
                    $('#jmhhgtranshd').val(response.b);
                    $('#jmhanggarantrans').autoNumeric('set', response.a);
                    $('#jmhanggarantranshd').val(response.a);
                    $('#lastmodaltrans').autoNumeric('set', response.c);
                    $('#lastmodaltranshd').val(response.c);
                    $('#transmodal').modal('show').on('shown.bs.modal');
                    $.unblockUI();
                }
                    
            }
        });
}
 $('input[name="jmhhgbeli"]').change(function () {
    var jmhhgbeli = $(this).autoNumeric('get');
    if (!$(this).autoNumeric('get')){
        var jmhhgbeli = 0;
        $('input[name="jmhhgbeli"]').val(jmhhgbeli);
    }
     $('input[name="jmhhgbelihd"]').val(jmhhgbeli);
});

$(document).ready(function() {
     $('#jmhhgbeli').on('change', function() {
        // Set value for fields
        var hargadata3=0;
        var hargadata0 = $('input[name="jmhhgbelihd"]').val();
        var hargadata1 = $('input[name="jmhhgtranshd"]').val();
        var hargadata2 = $('input[name="jmhanggarantranshd"]').val();
        var hargadata3=parseInt(hargadata2)-(parseInt(hargadata0)+parseInt(hargadata1));
        $('input[name="lastmodaltranshd"]').val(hargadata3);
        $('input[name="lastmodaltrans"]').autoNumeric('set', hargadata3);
                                

        // Revalidate the fields
        $('#modtransbelanja').formValidation('revalidateField', 'jmhhgbelihd');
    });
    $('#datepicker')
        .datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true,
        })
        .on('changeDate', function(e) {
            // Revalidate the date field
            $('#modtransbelanja').formValidation('revalidateField', 'tglbeli');
        });
        $('#modtransbelanja').formValidation({
        framework: 'bootstrap',
        excluded: ':disabled',
        icon: {
            validating: 'glyphicon glyphicon-refresh'
        },
        row: {
            valid: 'field-success',
            invalid: 'field-error'
        },
        locale: 'id_ID',
        fields: {
            tglbeli: {
                validators: {
                        notEmpty: {

                        },
                        date: {
                            format: 'DD/MM/YYYY'
                        }
                }
            },
            ketbelanjatrans: {
                validators: {
                    notEmpty: {
                        
                    }
                }
            },
            buyer: {
                validators: {
                    notEmpty: {
                        
                    }
                }
            },
            jmhhgbelihd: {
                validators: {
                    notEmpty: {
                        
                    },
                    callback: {
                            message: 'Jumlah pembelian Terlampau Besar !!!!',
                            callback: function(value, validator, $field) {
                                var hargadata0 = $('input[name="jmhhgbelihd"]').val();
                                var hargadata1 = $('input[name="jmhhgtranshd"]').val();
                                var hargadata2 = $('input[name="jmhanggarantranshd"]').val();
                                var hargadata3=0;
                                var hargadata3=parseInt(hargadata0)+parseInt(hargadata1);
                                if(hargadata3 > hargadata2){
                                    return {
                                        valid: false
                                    }
                                }
                                return true;
                            }
                        }
                }
            }


        }
    })
        .on('err.validator.fv', function(e, data) {
            if (data.field === 'jmhhgbelihd') {
                data.element
                    .data('fv.messages')
                    .find('.help-block[data-fv-for="' + data.field + '"]').hide()
                    .filter('[data-fv-validator="' + data.validator + '"]').show();
            }
        })
    
       .on('success.form.fv', function(e) {
        e.preventDefault();
        $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
        $.ajax({
            type    : 'POST',
            url     : url_link+'daftar-kegiatan/belanja-data-addbuy',
            data    : $('#modtransbelanja').serialize(),
            dataType: 'json',
            async: true,
            success : function(response){
                if(response.msg == 'success'){
                    reload_table();
                    $('#transmodal').modal('hide').on('shown.bs.modal');
                    $.unblockUI();
                    toastr.success('Transaction has been success !');
                }else{
                    $('#transmodal').modal('hide').on('shown.bs.modal');
                    $.unblockUI();
                    toastr.error('Transaction has been Fail !');                 
                }
            }
        });
    });
});