var table;
$('.datepickermonth').datepicker({
    autoclose: true,
    format: 'mm',
    todayHighlight    :true,
    minViewMode: 1,
}).on('changeDate', function(e) {
        reload_table();
    });
$(document).ready(function() {
    table = $('#data_transaksi').DataTable( {
        'searching': true,
        'paging':   true,
        'ordering': false,
        'info':     false,
        'processing': true,
        'serverSide': true,
        'ajax': {
            'url': 'data-transaksi/tabel-view',
            'type': 'POST',
            "data":  function ( data ) {
                data.bulantampil = $('input[name="printmonthy"]').val();
            }

        },
        'columnDefs': [
                        {
                            'targets': 1,
                            'createdCell':  function (td, cellData, rowData, row, col) {
                                $(td).attr('cr_tgl', 'cell-' + cellData); 
                            }
                        },
                        {
                            'targets': 2,
                            'createdCell':  function (td, cellData, rowData, row, col) {
                                $(td).attr('cr_pener', 'cell'); 
                            }
                        },
                         {
                            'targets': 3,
                            'createdCell':  function (td, cellData, rowData, row, col) {
                                $(td).attr('cr_urai', 'cell'); 
                            }
                        },
                        {
                            'targets': 4,
                            'createdCell':  function (td, cellData, rowData, row, col) {
                                $(td).attr('cr_jumh', 'cell'); 
                            }
                        },
        ],
        
         responsive: true,
        'language': {
    'sEmptyTable':     'No data available in table',
    'sInfo':           'Showing _START_ to _END_ of _TOTAL_ entries',
    'sInfoEmpty':      'Showing 0 to 0 of 0 entries',
    'sInfoFiltered':   '(filtered from _MAX_ total entries)',
    'sInfoPostFix':    '',
    'sInfoThousands':  ',',
    'sLengthMenu':     'Show _MENU_ entries',
    'sLoadingRecords': 'Loading...',
    'sProcessing':     'Processing...',
    'sSearch':         'Search:',
    'sZeroRecords':    'No matching records found',
    'oPaginate': {
        'sFirst':    'First',
        'sLast':     'Last',
        'sNext':     'Next',
        'sPrevious': 'Previous'
    },
    'oAria': {
        'sSortAscending':  ': activate to sort column ascending',
        'sSortDescending': ': activate to sort column descending'
    }
}
    } );
 return set_tok(csfrData);
});
function reload_table(){
      table.ajax.reload(null,false);
}
$('#refreshya').click(function(){
    $('input[name="printmonthy"]').val('')
reload_table();
});
function tinfotrans(ot){
     $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    //var rssId = $(this).children('button').attr('id');
    var $row = $('.info'+ot).closest('tr');
    var $text = $row.find('[cr_tgl]').text();
    var $textpener = $row.find('[cr_pener]').text();
    var $texturaian = $row.find('[cr_urai]').text();
    var $textjumh = $row.find('[cr_jumh]').text();
    $('.tgltrans').empty();
    $('.penertrans').empty();
    $('.uraitrans').empty();
    $('.tgltrans').text($text);
    $('.penertrans').text($textpener);
    $('.uraitrans').text($texturaian);
    $('.jmhtrans').text($textjumh);
    $('#viewtrans').modal('show').on('shown.bs.modal');
    $.unblockUI(); 
}
function deldat(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    var $row = $('.deldata'+ot).closest('tr');
    var $text = $row.find('[cr_tgl]').text();
    var $texturaian = $row.find('[cr_urai]').text();
    $('#text').empty();
    $('#text').append('<span style="font-size:5rem;color:#f0ad4e;"><i class="fa fa-fw fa-trash-o" style="cursor: unset !important"></i></span><br/><span style="font-size: 1.75rem;color: #818a91 !important;">'+$text+'</span><br/><span style="font-size: 1.25rem;color: #818a91 !important;">'+$texturaian+'</span>');
    $('div.modal-body #confdel').attr('onclick','confdel("'+ot+'")');
    $('#deltrans').modal('show').on('shown.bs.modal');
    $.unblockUI();
};
function confdel(ot){
    $.blockUI({ message: '<div id="fountainTextG"><div id="fountainTextG_1" class="fountainTextG">L</div><div id="fountainTextG_2" class="fountainTextG">o</div><div id="fountainTextG_3" class="fountainTextG">a</div><div id="fountainTextG_4" class="fountainTextG">d</div><div id="fountainTextG_5" class="fountainTextG">i</div><div id="fountainTextG_6" class="fountainTextG">n</div><div id="fountainTextG_7" class="fountainTextG">g</div><div id="fountainTextG_8" class="fountainTextG"> </div><div id="fountainTextG_9" class="fountainTextG">.</div><div id="fountainTextG_10" class="fountainTextG">.</div><div id="fountainTextG_11" class="fountainTextG">.</div></div>' });
    $('#deltrans').modal('hide').on('shown.bs.modal');
    $.ajax({
        type    : 'POST',
        url     : 'data-transaksi/delete-data',
        data    : {take:ot},
        dataType: 'json',
        async: false,
        success : function(response){
            if(response.msg == true){
                reload_table();
                $.unblockUI();
                toastr.success('Delete has been success !');
            }else{
                $.unblockUI();
                toastr.error('Delete has been Fail !');                 
            }
        }
    });
};
