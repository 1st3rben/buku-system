-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2017 at 09:17 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dpa_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `sys_belanja`
--

CREATE TABLE `sys_belanja` (
  `id_belanja` int(5) NOT NULL,
  `id_kegiatan` int(5) NOT NULL,
  `code_belanja` char(8) NOT NULL,
  `ket` text NOT NULL,
  `prev_modal` decimal(65,0) NOT NULL,
  `transaksi` decimal(65,0) NOT NULL,
  `last_modal` decimal(65,0) NOT NULL,
  `c_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `u_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sys_kegiatan`
--

CREATE TABLE `sys_kegiatan` (
  `id_kegiatan` int(5) NOT NULL,
  `code_dpk` decimal(11,0) NOT NULL,
  `uraian_kegiatan` text NOT NULL,
  `anggaran` decimal(65,0) NOT NULL,
  `tot_pengeluaran` decimal(65,0) NOT NULL,
  `sisa_anggaran` decimal(65,0) NOT NULL,
  `c_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `u_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sys_log`
--

CREATE TABLE `sys_log` (
  `id_login` int(5) NOT NULL,
  `id_user` int(5) NOT NULL,
  `pass_log` varchar(255) NOT NULL COMMENT 'MD5();\r\n',
  `key_uppass` varchar(255) NOT NULL,
  `act_key` varchar(255) NOT NULL,
  `level` float(5,0) NOT NULL,
  `status` float(5,0) NOT NULL,
  `c_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_log`
--

INSERT INTO `sys_log` (`id_login`, `id_user`, `pass_log`, `key_uppass`, `act_key`, `level`, `status`, `c_date`, `u_date`) VALUES
(2, 2, 'df7fb7c65949e480fabb73f574a8581b', '520', 'EB1HwPu8rMYu0JXsKBkC4nQQZkqg3JQaLXvilDj1ggJKVpe915fbIdc3t8nMgjirXrIHRHC9vK-hGLvuFZWERg', 0, 0, '2017-04-01 12:32:37', '2017-07-01 07:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `sys_manag`
--

CREATE TABLE `sys_manag` (
  `id_manag` int(5) NOT NULL,
  `menu` varchar(255) NOT NULL,
  `icon` text NOT NULL,
  `status` float(5,0) NOT NULL DEFAULT '0',
  `c_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sys_manag`
--

INSERT INTO `sys_manag` (`id_manag`, `menu`, `icon`, `status`, `c_date`, `u_date`) VALUES
(6, 'Daftar Kegiatan', '<i class="fa fa-table"></i>', 0, '2017-04-01 12:31:35', '2017-04-01 20:34:41'),
(8, 'Data Transaksi', '<i class="fa fa-cart-plus"></i>', 0, '2017-04-08 10:49:48', '2017-04-08 10:49:48'),
(9, ' Settings', '<i class="fa fa-gear"></i>', 1, '2017-04-11 13:07:01', '2017-04-11 13:07:01');

-- --------------------------------------------------------

--
-- Table structure for table `sys_profile`
--

CREATE TABLE `sys_profile` (
  `id_user` int(5) NOT NULL,
  `nm_user` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `c_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sys_profile`
--

INSERT INTO `sys_profile` (`id_user`, `nm_user`, `email`, `c_date`, `u_date`) VALUES
(2, 'root', 'adhytsa18@gmail.com', '0000-00-00 00:00:00', '2017-04-01 12:23:05');

-- --------------------------------------------------------

--
-- Table structure for table `sys_transaksi`
--

CREATE TABLE `sys_transaksi` (
  `id_transaksi` int(5) NOT NULL,
  `id_belanja` int(5) NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `penerima` varchar(255) NOT NULL,
  `uraian` text NOT NULL,
  `total_transaksi` decimal(65,0) NOT NULL,
  `c_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `u_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sys_belanja`
--
ALTER TABLE `sys_belanja`
  ADD PRIMARY KEY (`id_belanja`,`id_kegiatan`),
  ADD KEY `kegiatan` (`id_kegiatan`),
  ADD KEY `id_belanja` (`id_belanja`);

--
-- Indexes for table `sys_kegiatan`
--
ALTER TABLE `sys_kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indexes for table `sys_log`
--
ALTER TABLE `sys_log`
  ADD PRIMARY KEY (`id_login`,`id_user`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_login` (`id_login`);

--
-- Indexes for table `sys_manag`
--
ALTER TABLE `sys_manag`
  ADD PRIMARY KEY (`id_manag`);

--
-- Indexes for table `sys_profile`
--
ALTER TABLE `sys_profile`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `sys_transaksi`
--
ALTER TABLE `sys_transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `transaksi ke belanja` (`id_belanja`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sys_belanja`
--
ALTER TABLE `sys_belanja`
  MODIFY `id_belanja` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `sys_kegiatan`
--
ALTER TABLE `sys_kegiatan`
  MODIFY `id_kegiatan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `sys_log`
--
ALTER TABLE `sys_log`
  MODIFY `id_login` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `sys_manag`
--
ALTER TABLE `sys_manag`
  MODIFY `id_manag` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sys_profile`
--
ALTER TABLE `sys_profile`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `sys_transaksi`
--
ALTER TABLE `sys_transaksi`
  MODIFY `id_transaksi` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `sys_belanja`
--
ALTER TABLE `sys_belanja`
  ADD CONSTRAINT `kegiatan` FOREIGN KEY (`id_kegiatan`) REFERENCES `sys_kegiatan` (`id_kegiatan`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `sys_log`
--
ALTER TABLE `sys_log`
  ADD CONSTRAINT `user` FOREIGN KEY (`id_user`) REFERENCES `sys_profile` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sys_transaksi`
--
ALTER TABLE `sys_transaksi`
  ADD CONSTRAINT `transaksi ke belanja` FOREIGN KEY (`id_belanja`) REFERENCES `sys_belanja` (`id_belanja`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
