<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdpakegiatan extends CI_Model {
    var $table= 'sys_kegiatan';
    var $column = array('id_kegiatan','code_dpk','uraian_kegiatan'
        );
    var $order = array('code_dpk' => 'asc');
	function __constuct()
	{
		parent::__constuct(); 
		loader::database();    
	}
	private function _get_datatables_query(){
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column as $item)
        {
            if($_POST['search']['value'])
                ($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
            $column[$i] = $item;
            $i++;
        }
 
        if(isset($_POST['order']))
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all(){	
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function get_codekgtn($data){
        $this->db->cache_on();
        $this->db->where($data['where'],$data['id']);
        $result=$this->db->get($this->table);
        $this->db->cache_off();
        return $result;
    }

}