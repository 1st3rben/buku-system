<?php
$route['daftar-kegiatan'] = 'Dpakegiatan/index';
$route['daftar-kegiatan/tabel-view'] = 'Dpakegiatan/ajax_list';
$route['daftar-kegiatan/data-auths'] = 'Dpakegiatan/check_datakegiatan';
$route['daftar-kegiatan/data-auths-edit'] = 'Dpakegiatan/check_datakegiatanedit';
$route['daftar-kegiatan/data-add'] = 'Dpakegiatan/add_datakegiatan';
$route['daftar-kegiatan/data-save'] = 'Dpakegiatan/save_kegiatan';
$route['daftar-kegiatan/data-edit'] = 'Dpakegiatan/show_datakegiatan';
$route['daftar-kegiatan/data-update'] = 'Dpakegiatan/update_datakegiatan';
$route['daftar-kegiatan/data-delete'] = 'Dpakegiatan/delete_datakegiatan';
?>
