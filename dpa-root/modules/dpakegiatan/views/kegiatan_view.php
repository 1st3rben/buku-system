  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$text?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?=$text?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title"> </h3>
              <div class="pull-right box-tools">
                <button type="button" id="addata" class="btn btn-success btn-sm" data-toggle="tooltip" title="Add Data">
                  <i class="fa fa-plus"></i></button>
              </div> 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="daftar_kegiatan" class="table dt-responsive table-striped table-bordered nowrap" width="100%" cellspacing="0">
                <thead>
            <tr>
              <th width="5%">No</th>
              <th width="13%">Kode Kegiatan</th>
              <th class="ukeg">Uraian Kegiatan</th>
              <th width="10%">Action</th>
            </tr>
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<div class="modal fade" id="addmodal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah <?=$text?></h4>
      </div>
       <?php $attributes = array('role'=>'form','id'=>'modaddkegiatan','class'=>'form-horizontal');
       echo form_open('',$attributes);
       ?>
      <div class="modal-body">
          <div class="form-group">
            <label class="col-sm-2 control-label">Kode Kegiatan</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="cdkegiatan" placeholder="Kode Kegiatan" maxlength="11"/>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Uraian Kegiatan</label>
            <div class="col-sm-10">
              <textarea class="form-control" cols="2" rows="2" name="ketkegiatan" placeholder="Uraian Kegiatan"></textarea>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      <?php echo form_close(); ?>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="editmodal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Edit <?=$text?></h4>
            </div>
             <?php $attributes = array('role'=>'form','id'=>'modeditkegiatan','class'=>'form-horizontal');
                    echo form_open('',$attributes);
              ?>
              <div class="modal-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Kode Kegiatan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="editcdkegiatan" name="editcdkegiatan" placeholder="Kode Kegiatan" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Uraian Kegiatan</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" cols="2" rows="2" id="editketkegiatan" name="editketkegiatan" placeholder="Uraian Kegiatan"></textarea>
                  </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">Jmh Anggaran</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control autonumber" data-a-sign="Rp. " id="editjmhanggaran" name="editjmhanggaran" placeholder="Rp. 0,00"/>
                      <input type="hidden" class="form-control autonumber" name="editjmhanggaranhd" />
                  </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">Total Pengeluaran </label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control autonumber" data-a-sign="Rp. "  disabled="disabled" id="edittotpengeluaran" name="edittotpengeluaran" />
                  </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">Sisa Jumlah Anggaran </label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control autonumber" data-a-sign="Rp. "  disabled="disabled" id="editssanggaran" name="editssanggaran" />
                  </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">Tanggal </label>
                  <div class="col-sm-10">

                      <input type="text" class="form-control autonumber" data-a-sign="Rp. "  disabled="disabled" id="edittglkgtn" name="edittglkgtn" />
                  </div>
                </div>
            </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
          <?php echo form_close(); ?>

        </div>
    </div>
</div>
<div class="modal fade modal-flex" id="delmodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body" style="text-align: center">
          <div id="text" ></div>
          <br/>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="confdel">Ya, Hapus !</button>
          </div>
        </div>
    </div>
</div>
