<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dpakegiatan extends MX_Controller {
	public $data = array(
			'title'     => 'DPA',
			'text'     => 'Daftar Kegiatan',
			'link'    => 'daftar-kegiatan',
			'author'    => 'Adi.Sh0x',
			'js_system' =>	'syskeg.min.js'
		);
	public function __construct(){
        parent::__construct();
    	if(!sh0x_get_in()){
			redirect('system-login');
        }
    }
	public function index(){
			$this->_kegiatan();
	}
	private function _kegiatan(){
			$view='dpakegiatan/kegiatan_view';
			$this->dpalib->template_rt($view,$this->data);
	}
	function ajax_list() {
	        $list = $this->kegiatan->get_datatables();
	        $data = array();
	        $no = $_POST['start']+1;
	        foreach ($list as $person) {
	            $row = array();
	            $row[] = $no++;
	            $row[] = $person->code_dpk;
	            $uraian = stripslashes($person->uraian_kegiatan);
	            $row[] = $uraian;
	            $id    = $this->dpalib->enhex($person->id_kegiatan.','.$person->code_dpk);
	            $id_link    = $this->dpalib->encode($person->id_kegiatan);
	            $code_pk=$person->code_dpk;
	            $delete_area=$id.','.$code_pk.','.base64_encode($uraian);
	            $row[] = '<a class="btn btn-xs btn-info" href="'.site_url('daftar-kegiatan/belanja/'.$id_link.'').'" ><i class="fa fa-object-group"></i></a>
                          <button type="button" id="edit-data'.$id.'" onclick=javascript:eddat("'.$id.'") class="btn btn-xs btn-success waves-effect waves-light" data-toggle="tooltip" title="Edit Data" ><i class="fa fa-edit"></i>
                                </button>
                           <button type="button" class="deldata'.$id.' btn btn-xs btn-danger waves-effect waves-light" id="daftar_kegiatanbt" onclick=javascript:deldat("'.$id.'") data-toggle="tooltip" title="Delete Data"><i class="fa fa-trash"></i>
                                </button>';
	 
	            $data[] = $row;
	        }
	        $output = array(
	                        "draw" => $_POST['draw'],
	                        "recordsTotal" => $this->kegiatan->count_all($this->data),
	                        "recordsFiltered" => $this->kegiatan->count_filtered($this->data),
	                        "data" => $data,
	                );
	        echo json_encode($output);
	    
    }
    function add_datakegiatan(){
    		$msg=$this->load->view('dpakegiatan/add_kegiatan',$this->data);
    }
    function save_kegiatan(){
    	    $this->form_validation->set_rules('cdkegiatan', '', 'trim|required|exact_length[11]');
    		$this->form_validation->set_rules('ketkegiatan', '', 'trim|required');
    		if($this->form_validation->run() == TRUE){
    			$this->data['cdkegiatan']=$this->db->escape_str($this->input->post('cdkegiatan',TRUE));
    			$this->data['ketkegiatan']=$this->db->escape_str($this->input->post('ketkegiatan',TRUE));
    			$input = array(
							'code_dpk' => $this->data['cdkegiatan'],
							'uraian_kegiatan'=> $this->data['ketkegiatan'],
							'anggaran' =>'0' ,
							'tot_pengeluaran' => '0' ,
							'sisa_anggaran' => '0' ,
							'c_date' => date('Y-m-d H:i:s',now()),
							'u_date' => date('Y-m-d H:i:s',now())
						);
    			$msg    = "success";
    			$insert = $this->db->insert('sys_kegiatan',$input);
				if($insert){
	                $msg    = "success";
	                $this->db->cache_delete_all();
            	}else{
            		$msg    = "error";
            	}
    		}
    		echo json_encode(array('msg'=>$msg));
    }
    function update_datakegiatan(){
    	    $this->form_validation->set_rules('editcdkegiatan', '', 'trim|required|exact_length[11]');
    		$this->form_validation->set_rules('editketkegiatan', '', 'trim|required');
    		$this->form_validation->set_rules('editjmhanggaranhd', '', 'trim');
    		if($this->form_validation->run() == TRUE){
    			$this->data['cdkegiatan']=$this->db->escape_str($this->input->post('editcdkegiatan',TRUE));
    			$this->data['ketkegiatan']=$this->db->escape_str($this->input->post('editketkegiatan',TRUE));
    			$this->data['jmhanggaran']=$this->db->escape_str($this->input->post('editjmhanggaranhd',TRUE));
    			$data_edit = array(
							'code_dpk' => $this->data['cdkegiatan'],
							'uraian_kegiatan'=> $this->data['ketkegiatan'],
							'anggaran' => $this->data['jmhanggaran'] ,
							'u_date' => date('Y-m-d H:i:s',now())
						);
    			$msg    = "success";
    			$update = $this->db->where("id_kegiatan",$this->session->userdata('sesid_kegiatan'))->update('sys_kegiatan',$data_edit);
				if($update){
	                $msg    = "success";
	                $this->cache->clean();
	                $this->db->cache_delete_all();
            	}else{
            		$msg    = "error";
            	}
    		}
    		echo json_encode(array('msg'=>$msg));
    }
    function edit_datakegiatan(){
    	$msg=$this->load->view('dpakegiatan/add_kegiatan',$this->data);
    }
    function check_datakegiatan(){
    	$id_get	=$this->db->escape_str($this->input->post('cdkegiatan',TRUE));
		$this->data['id']    = $id_get;
		$this->data['where']    = 'code_dpk';
		$list = $this->kegiatan->get_codekgtn($this->data);
		if($list->num_rows()>0){
			$msg=false;
		}else{
			$msg = true;
		}
		echo json_encode(array('valid'=>$msg));
    }

    function check_datakegiatanedit(){
    	$id_get	=$this->db->escape_str($this->input->post('editcdkegiatan',TRUE));
		$this->data['id']    = $id_get;
		$this->data['where']    = 'code_dpk';
		$id_kgt=$this->session->userdata('sessremotecode_dpk');
		$list = $this->kegiatan->get_codekgtn($this->data);
		if($list->num_rows()>0){
			$get_data=$list->row();
			if($id_get == $id_kgt){
				$msg = true;
			}else{
				$msg=false;	
			}
		}else{
			$msg = true;
		}
		echo json_encode(array('valid'=>$msg));
    }
    function show_datakegiatan(){
    	$id_get	=$this->db->escape_str($this->input->post('take',TRUE));
    	$datestring 			= 	"%d %M %Y %h:%i:%s";
    	$id_kgtn = explode(',',$this->dpalib->dehex($id_get));
		$this->data['id']    = $id_kgtn[0];
		$this->data['remotecode_dpk']    = $id_kgtn[1];
		$this->data['where']	= 'id_kegiatan';
		$load_chakgtedt='is_kegiatan_'.$id_get;
		$datestring = 'Year: %Y Month: %m Day: %d - %h:%i %a';
		 if ( !$check_data = $this->cache->get($load_chakgtedt)){
		 	$check_data=$this->kegiatan->get_codekgtn($this->data);
			if ($check_data->num_rows() > 0 ){
				$msg=true;
				$check_data=$check_data->row();
				$code_dpk=$check_data->code_dpk;
				$uraian_kegiatan=$check_data->uraian_kegiatan;
				$anggaran=$check_data->anggaran;
				$tot_pengeluaran=$check_data->tot_pengeluaran;
				$sisa_anggaran=$check_data->sisa_anggaran;
				$u_date	=date("d M Y H:i:s", strtotime($check_data->u_date));
				$sess_data['sessremotecode_dpk']	=	$this->data['remotecode_dpk'];
				$sess_data['sesid_kegiatan']	=	$this->data['id'] ;
        		$this->session->set_userdata($sess_data);
				$this->cache->save($load_chakgtedt, $check_data, 300);
			}else{
				$msg=false;
				$code_dpk='';
				$uraian_kegiatan='';
				$anggaran='';
				$tot_pengeluaran='';
				$sisa_anggaran='';
				$u_date='';
			}
		 }else{
		 	$get_data = $this->cache->get($load_chakgtedt);
		 	$msg=true;
			$code_dpk=$get_data->code_dpk;
			$uraian_kegiatan=$get_data->uraian_kegiatan;
			$anggaran=$get_data->anggaran;
			$tot_pengeluaran=$get_data->tot_pengeluaran;
			$sisa_anggaran=$get_data->sisa_anggaran;
			$u_date	=date("d M Y H:i:s", strtotime($get_data->u_date));
			$sess_data['sesid_kegiatan']	=	$this->data['id'] ;
        	$this->session->set_userdata($sess_data);
		 }
		echo json_encode(array('msg'=>$msg,'a'=>$anggaran,'b'=>$tot_pengeluaran,'c'=>$sisa_anggaran,'d'=>$u_date));
    }
    function delete_datakegiatan(){
    	$id_get	=$this->db->escape_str($this->input->post('take',TRUE));
    	$load_chakgtedt='is_kegiatan_'.$id_get;
    	$id_kgtn = explode(',',$this->dpalib->dehex($id_get));
		$this->data['id']    = $id_kgtn[0];
		$this->data['where']	= 'id_kegiatan';
		if ( !$check_data = $this->cache->get($load_chakgtedt)){
			$check_data = $this->kegiatan->get_codekgtn($this->data);
			if($check_data->num_rows()>0){
				$delete=$this->db->where_in('id_kegiatan',$this->data['id'])->delete('sys_kegiatan');
				$this->db->cache_delete_all();
				$this->cache->delete($load_chakgtedt);
				$msg=true;
			}else{
				$msg=false;
			}
		}else{
			$get_dat= $this->cache->get($load_chakgtedt);
			$delete=$this->db->where_in('id_kegiatan',$this->data['id'])->delete('sys_kegiatan');
			$this->db->cache_delete_all();
			$this->cache->delete($load_chakgtedt);
			$msg=true;
		}
		echo json_encode(array('msg'=>$msg));
    }
}