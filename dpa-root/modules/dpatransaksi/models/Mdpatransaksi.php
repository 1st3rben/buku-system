<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdpatransaksi extends CI_Model {
    var $table= 'sys_transaksi';
    var $column_order = array('id_transaksi','id_belanja','tgl_transaksi','penerima','uraian','total_transaksi'
                        //,'total_transaksi','c_date','u_date'
                        );
    var $column_search = array('tgl_transaksi','penerima','uraian','total_transaksi'); 
    var $order = array('id_transaksi' => 'asc');
	function __constuct()
	{
		parent::__constuct();  // Call the Model constructor 
		loader::database();    // Connect to current database setting.
	}
	private function _get_datatables_query(){    
        if($this->input->post('bulantampil')){
            $sekarang=$this->input->post('bulantampil');
            $where="MONTH(tgl_transaksi)='$sekarang'";
            $this->db->where($where);
            $sess_data['bullanprint']  = $sekarang;
            $this->session->set_userdata($sess_data);            
        }
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
 
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        //$this->db->order_by('emmail','asc');
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all(){	
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function update_kegtotsis($data) {
        return $this->db->query( 
        "update sys_kegiatan SK 
        JOIN (SELECT SB.id_kegiatan ,SUM(SB.transaksi) AS sum_trans, SUM(SB.last_modal) AS sum_last_modal
                FROM sys_belanja SB 
                WHERE SB.id_kegiatan=".$data."
        )
        SJ
        ON SJ.id_kegiatan = SK.id_kegiatan
        SET SK.tot_pengeluaran = SJ.sum_trans,
            SK.sisa_anggaran = SJ.sum_last_modal
        ");
    }
    function selid_kegiatan($data) {
        $this->db->select('transaksi,last_modal,id_kegiatan');
        $this->db->where('id_belanja',$data['id_belanja']);
        return $this->db->get('sys_belanja');
    }
    function belanjajum() {
        $this->db->select('*');
        $this->db->from('sys_transaksi');
        $this->db->join('sys_belanja', 'sys_transaksi.id_belanja = sys_belanja.id_belanja');
        return $this->db->get();
    }
    function get_report(){
        $sekarang='';
        if(!empty($this->session->userdata('bullanprint'))){
            $sekarang=$this->session->userdata('bullanprint');
        }
        $where="MONTH(sys_transaksi.tgl_transaksi)='$sekarang'";
        $this->db->select('sys_transaksi.id_belanja, sys_transaksi.tgl_transaksi,sys_transaksi.penerima,sys_transaksi.uraian,sys_transaksi.total_transaksi,sys_belanja.code_belanja,sys_kegiatan.code_dpk');
        $this->db->from('sys_transaksi');
        $this->db->join('sys_belanja', 'sys_transaksi.id_belanja = sys_belanja.id_belanja');
        $this->db->join('sys_kegiatan', 'sys_belanja.id_kegiatan = sys_kegiatan.id_kegiatan');
        $this->db->where($where);
        $this->db->order_by('sys_belanja.code_belanja', 'asc');
        $query = $this->db->get();
        return $query;
    }
}