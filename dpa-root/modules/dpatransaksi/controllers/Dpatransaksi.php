<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dpatransaksi extends MX_Controller {
	public $data = array(
			'title'     => 'DPA',
			'text'     => 'Data Transaksi',
			'link'    => 'data-transaksi',
			'author'    => 'Adi.Sh0x',
			'js_system' =>	'systrans.js'
		);
	public function __construct(){
        parent::__construct();
    	if(!sh0x_get_in()){
			redirect('system-login');
        }
    }
	public function index(){
		$this->_dashboard();
		/*$this->form_validation->set_rules('printmonthy', '', 'trim|required');
		$this->form_validation->set_rules('printyears', '', 'trim|required');
		if($this->form_validation->run() == TRUE){
			if(!empty($_POST['printmonthy'])){
				$bulan=$this->db->escape_str($this->input->post('printmonthy',TRUE));
				$this->drreportm($bulan);
			}elseif(!empty($_POST['printmonthy'])){
				$bulan=$this->db->escape_str($this->input->post('printyears',TRUE));
				$this->drreporty($bulan);
			}else{
				$this->_dashboard();
			}
		}*/
	}
	private function _dashboard(){
		$view='dpatransaksi/transaksi_view';
		$this->dpalib->template_rt($view,$this->data);
	}
	function ajax_list() {
	        $list = $this->transaksi->get_datatables();
	        $data = array();
	        $no = $_POST['start']+1;
	        foreach ($list as $person) {
	            
	            $row = array();
	            $row[] = $no++;
	            $datetrans  = date('d F Y', strtotime($person->tgl_transaksi));
	            $row[] = $datetrans ;
	            $row[] = stripslashes($person->penerima);
	            $row[] = stripslashes($person->uraian);
	            $row[] = sprintf('Rp %s',number_format($person->total_transaksi,2,",","."));
	            $id    = $this->dpalib->enhex($person->id_transaksi.','.$person->tgl_transaksi.','.$person->id_belanja.','.$person->total_transaksi);
	            $row[] = '
	            <button type="button" class="btn btn-xs btn-info info'.$id.'" onclick=javascrip:tinfotrans("'.$id.'")><i class="fa fa-info-circle"></i>
                </button>
                <button type="button" class="btn btn-xs btn-danger deldata'.$id.'" onclick=javascript:deldat("'.$id.'") data-toggle="tooltip" data-placement="top" title="" data-original-title=".btn-mini"><i class="fa fa-trash"></i>
                </button>';
	            $data[] = $row;
	        }
	 
	        $output = array(
	                        "draw" => $_POST['draw'],
	                        "recordsTotal" => $this->transaksi->count_all($this->data),
	                        "recordsFiltered" => $this->transaksi->count_filtered($this->data),
	                        "data" => $data,
	                );
	        echo json_encode($output);
	    
    }
    function delete_datatransaksi(){
    	$id_get	=$this->db->escape_str($this->input->post('take',TRUE));
    	$id_prim = explode(',',$this->dpalib->dehex($id_get));
		$this->data['id_trans']    = $id_prim[0];
		$this->data['id_belanja']    = $id_prim[2];
		$this->data['total_transaksi']    = $id_prim[3];
		$angka=$this->transaksi->selid_kegiatan($this->data);
		$angakahasil=$angka->row();
		$idkgt=$angakahasil->id_kegiatan;
		$transaksi=$angakahasil->transaksi;
		$last_modal=$angakahasil->last_modal;
		$transnew=$transaksi-$this->data['total_transaksi'];
		$last_modalnew=$last_modal+$this->data['total_transaksi'];
		$data_edit = array(
							'transaksi' => $transnew,
							'last_modal'=> $last_modalnew
						);
    			$update = $this->db->where("id_belanja",$this->data['id_belanja'])->update('sys_belanja',$data_edit);
    			$delete=$this->db->where_in('id_transaksi',$this->data['id_trans'])->delete('sys_transaksi');
    			$update_prevs=$this->transaksi->update_kegtotsis($idkgt);
    			if($delete){
	                $msg    = true;
	                $this->cache->clean();
	                $this->db->cache_delete_all();
            	}else{
            		$msg    =false;
            	}
            	echo json_encode(array('msg'=>$msg));
    }
    function prita(){
    	$this->data['cetak']=$this->transaksi->get_report();
    	$this->data['belanja']=$this->transaksi->belanjajum();
		$html=$this->load->view('dpatransaksi/print_view',$this->data,true);
		$date = date('d-m-Y');
		$name = 'Laporan_';
		$filename=$name.$date;
		$paper = 'A4';
		$orientation = 'potrait';
		pdf_create($html, $filename, $paper, $orientation);
    }
}