<html lang="en">
<head>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:3px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:3px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
.tg .tg-6222{font-size:10px !important;font-family:Tahoma, Geneva, sans-serif !important;;text-align:center;vertical-align:top border: 1px solid #2a2a2a !important;}
.tg .tg-2gew{font-size:10px !important;font-family:Tahoma, Geneva, sans-serif !important;;vertical-align:top border: 1px solid #2a2a2a !important;}
.tg .tg-2to7{font-weight:bold;font-size:12px !important;font-family:Tahoma, Geneva, sans-serif !important;;text-align:center;vertical-align:top border: 1px solid #2a2a2a !important;}
.tg-2gew,.tg-2to7,.tg-6222{border: 1px solid #2a2a2a !important;}
.tg-2to7{font-size:12px !important;padding: 8px !important;}
.tg-2gew,.tg-6222{font-size:10px !important; padding: 5px !important;}
.divA{
    height: 1px;
    border-bottom: 1px solid black;
}
.divB{
    height: 1px;
    border-bottom: 2px solid black;
}
.cssline{font-size:10px;font-family:Arial, Helvetica, sans-serif !important;;text-align:left}
font-size: 12px;
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid #2a2a2a !important;
}
.table-bordered {
    border: 1px solid #2a2a2a;
}
</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>

    <span>Data Transaksi </span>
    <span>Per</span><span class="clsper"></span>
    <span></span>
    <div class="divA"></div>
    <div class="divB"></div>
    <br/>
            <table width="100%" class="table table-bordered" style="border-collapse: collapse;">
                <thead>
                    <tr>   
                        <th class="tg-2to7" width="5%">No</th>
                        <th class="tg-2to7" width="20%">Rek</th>
                        <th class="tg-2to7" width="15%">Tanggal</th>
                        <th class="tg-2to7" width="10%">Penerima</th>
                        <th class="tg-2to7" width="35%">Uraian</th>
                        <th class="tg-2to7" width="15%">Rp.</th>                   
                      
                    </tr>    
                </thead>
                <tbody class=".dataprint">
                    <?php
                    $no=1;
                    if(empty($cetak)){;?>
                    <tr><td class="tg-2gew">empty</td>
                            <td class="tg-2gew">empty</td>
                            <td class="tg-2gew">empty</td>
                            <td class="tg-2gew">empty</td>
                            <td class="tg-2gew">empty</td>
                            <td class="tg-2gew">empty</td>                          
                        </tr>
                    <?php }else{

                    foreach ($cetak->result() as $row) {
                        $datetrans  = date('d F Y', strtotime($row->tgl_transaksi));
                        $rup=sprintf('Rp %s',number_format($row->total_transaksi,2,",","."));
                        $code=$row->code_dpk.'.'.$row->code_belanja;
                        ?>
                        <tr><td class="tg-6222"><?php echo $no++; ?></td>
                            <td class="tg-2gew"><?php echo $code; ?></td>
                            <td class="tg-2gew"><?php echo $datetrans; ?></td>
                            <td class="tg-2gew"><?php echo $row->penerima; ?></td>
                            <td class="tg-2gew"><?php echo $row->uraian; ?></td>
                            <td class="tg-2gew"><?php echo $rup; ?></td>                          
                        </tr>
                    <?php }} ?> 
                </tbody>
            </table>
</body>
</html>