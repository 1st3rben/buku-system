  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$text?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?=$text?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box box-success">
          <div class="box-header">
              <h3 class="box-title"> </h3>
              <div class="col-xs-5 col-sm-2" style="margin: 5px;">
                <div class="input-group">
                    <input class="form-control datepickermonth" placeholder="mm/Y" type="text" name="printmonthy">
                  <span class="input-group-btn">
                    <a href="<?=site_url('print-data')?>" target="_blank" class="btn btn-default" type="submit">&nbsp;<i class="fa fa-print"></i>&nbsp;</a>
                  </span>
                </div><!-- /input-group -->
              </div>
              <div class="pull-right box-tools">
                <button type="button" id="refreshya" class="btn btn-success btn-sm" data-toggle="tooltip" title="Refresh">
                  <i class="fa fa-refresh"></i></button>
              </div> 
            </div>
            <!--/.box-header -->
            <div class="box-body">
          <table id="data_transaksi" class="table dt-responsive table-striped table-bordered nowrap" width="100%" cellspacing="0">
            <thead>
            <tr>
              <th width="5%">No</th>
              <th width="15%">Tgl Transaksi</th>
               <th width="15%">Penerima</th>
              <!--<th>Total Transaksi</th>-->
              <th>Uraian</th>
              <th>Jumlah</th>
              <th width="10%">Action</th>
            </tr>
            </thead>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="viewtrans" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Transaksi</span></h4>
          </div>
          <?php $attributes = array('role'=>'form','id'=>'trnsaksiview','class'=>'form-horizontal');
           echo form_open('',$attributes);
           ?>
          <div class="modal-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">Tanggal Transaksi</label>
                <div class="col-sm-10 control-labelr">
                  <span class="tgltrans"></span>
                </div>
            </div>
             <div class="form-group">
                <label class="col-sm-2 control-label">Penerima</label>
                <div class="col-sm-10 control-labelr">
                  <span class="penertrans"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Uraian</label>
                <div class="col-sm-10 control-labelr">
                  <span class="uraitrans"></span>
                </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jumlah Transaksi </label>
               <div class="col-sm-10 control-labelr">
                  <span class="jmhtrans"></span>
                </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
          </div>
          <?php echo form_close(); ?>
      </div>
  </div>
</div>
<div class="modal fade modal-flex" id="deltrans" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body" style="text-align: center">
          <div id="text" ></div>
          <br/>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="confdel">Ya, Hapus !</button>
          </div>
        </div>
    </div>
</div>