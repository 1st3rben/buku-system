<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dpadashboard extends MX_Controller {
	public $data = array(
			'title'     => 'DPA',
			'text'     => 'Dashboard',
			'author'    => 'Adi.Sh0x'
		);
	public function __construct(){
        parent::__construct();
    	if(!sh0x_get_in()){
			redirect('system-login');
        }
    }
	public function index(){
			$this->_dashboard();
	}
	private function _dashboard(){
		$view='dpadashboard/dashboard_view';
		$this->dpalib->template_rt($view,$this->data);
	}
	function dbbackup(){
	$this->load->dbutil();
	$time_now       = date('Y-m-d',time());
	$prefs = array(
			        //'tables'     => array('sys_belanja', 'sys_kegiatan', 'sys_log', 'sys_manag', 'sys_profile', 'sys_transaksi'),
			        'ignore'     => array(),
			        'format'     => 'txt',
			        'filename'   => 'DB-'.$time_now.'-backup.sql',
			        'add_drop'   => TRUE, 
			        'add_insert' => TRUE,
			        'newline'    => "\n"
			);
	$backup =& $this->dbutil->backup();
	write_file('/backup/DB-'.$time_now.'-backup.sql', $backup);
	force_download('DB-'.$time_now.'-backup.sql', $backup);
	}
}