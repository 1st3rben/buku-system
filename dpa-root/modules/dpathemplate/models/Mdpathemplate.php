<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdpathemplate extends CI_Model {
    var $parent = '0';
    var $hasil  = '';
    function __constuct(){
        parent::__constuct();
        loader::database();
    }
    function navigasi() {
        $this->db->cache_on();
        $this->db->select('*');
        $this->db->where('status','0');
        $result=$this->db->get('sys_manag');
        $this->db->cache_off();
        return $result; 
    }
    function userprof() {
        $this->db->cache_on();
        $this->db->select('*');
        $this->db->from('sys_profile');
        $this->db->join('sys_log', 'sys_log.id_user = sys_profile.id_user');
        $this->db->where('sys_profile.id_user',$this->session->userdata('wormood'));
        $result=$this->db->get();
        $this->db->cache_off();
        return $result; 
    }

}