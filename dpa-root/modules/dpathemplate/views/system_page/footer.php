  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <?php
    $time_now       = date('Y',time());
     ?>
    <strong>Copyright &copy; 2014-<?=$time_now?> <a href="https://sillysource.com">SillySource</a>.</strong> All rights
    reserved.
  </footer>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3.1.1 -->
<script src="<?=base_url()?>assets/plugins/jQuery/jquery-2.2.4.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<!--formvalidation-->
<script type="text/javascript" src="<?=base_url()?>assets/dist/js/formValidation.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/dist/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/dist/js/language/id_ID.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/dist/js/autoNumeric.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/dist/js/jquery.blockUI.js"></script>
<!-- Select2 -->
<script src="<?=base_url()?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<!-- script type="text/javascript" src="<?=base_url()?>assets/dist/js/modernizr.js"></script>SlimScroll -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script src="<?=base_url()?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- <script src="<?=base_url()?>assets/plugins/daterangepicker/daterangepicker.js"></script>

<script src="<?=base_url()?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
FastClick -->
<script src="<?=base_url()?>assets/plugins/fastclick/fastclick.js"></script>
<!-- Toastr -->
<script src="<?=base_url()?>assets/plugins/toastr/js/toastr.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/dist/js/system.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>assets/dist/js/demo.js"></script>

<script type="text/javascript">
var csfrData={};csfrData['<?php echo $this->security->get_csrf_token_name(); ?>']='<?php echo
$this->security->get_csrf_hash(); ?>';set_tok(csfrData);function set_tok(csfrData){$.ajaxSetup({data:csfrData})}
var url_link='<?=base_url()?>'
</script>
<?php if (!empty($js_system)){echo '<script src="'.base_url().'assets/dist/js/'.$js_system.'"></script>';}?>
<?php echo $this->session->flashdata('notif');?>
</body>
</html>