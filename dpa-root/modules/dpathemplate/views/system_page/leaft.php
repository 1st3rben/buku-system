  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="<?=site_url('dashboard')?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <?php 
                $is_navigasi=sh0x_get_system('navigasi');
                foreach ($is_navigasi->result() as $row){
                  $url_title = url_title($row->menu,$separator = '-',TRUE);
                echo '<li><a href="'.site_url($url_title).'">'.$row->icon.'<span>'.ucwords($row->menu).'</span></a></li>';
            };?>
            <?php if ($this->session->userdata('wormlevel')=='0'){?>
            <li><a href="<?=site_url('settings')?>"><i class="fa fa-gear"></i><span>Settings</span></a></li>
            <?php }?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>