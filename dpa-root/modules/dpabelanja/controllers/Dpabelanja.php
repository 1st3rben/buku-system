<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dpabelanja extends MX_Controller {
	public $data = array(
			'title'     => 'DPA',
			'text'     => 'Daftar Belanja',
			'link'    => 'daftar-belanja',
			'author'    => 'Adi.Sh0x',
			'js_system'=>'sysbel.min.js'
		);
	public function __construct(){
        parent::__construct();
    	if(!sh0x_get_in()){
			redirect('system-login');
        }
    }
	public function index(){
		$this->data['id_kegiatansubload'] = $this->dpalib->decode($this->uri->segment(3));
		$sess_data['id_kegiatansub']  = $this->data['id_kegiatansubload'];
        $this->session->set_userdata($sess_data);
        $this->data['kegiatan']	=	$this->belanja->bel_codekgtn($this->data);
        $view='dpabelanja/belanja_view';
		$this->dpalib->template_rt($view,$this->data);

	}
	function ajax_list() {
		$this->data['idview']=$this->session->userdata('id_kegiatansub');
	        $list = $this->belanja->get_datatables($this->data);
	        $data = array();
	        $no = $_POST['start']+1;
	        foreach ($list as $person) {
	            
	            $row = array();
	            $row[] = $no++;
	            $row[] = $person->code_belanja;
	            $uraian = stripslashes($person->ket);
	            $row[] = $uraian;
	            $id    = $this->dpalib->enhex($person->id_belanja.','.$person->code_belanja);
	            $row[] = '
	            <button type="button" id="edit-data'.$id.'" onclick=javascript:adtrans("'.$id.'") class="btn btn-xs btn-success" data-toggle="tooltip" title="Transaksi" ><i class="fa fa-shopping-cart"></i>
                </button>
                <button type="button" id="edit-data'.$id.'" onclick=javascript:eddat("'.$id.'") class="btn btn-xs btn-success" data-toggle="tooltip" title="Edit Data" ><i class="fa fa-edit"></i>
                </button>
                <button type="button" class="deldata'.$id.' btn btn-xs btn-danger" id="daftar_kegiatanbt" onclick=javascript:deldat("'.$id.'") data-toggle="tooltip" title="Delete Data"><i class="fa fa-trash"></i>
                </button>';
	 
	            $data[] = $row;
	        }
	 
	        $output = array(
	                        "draw" => $_POST['draw'],
	                        "recordsTotal" => $this->belanja->count_all($this->data),
	                        "recordsFiltered" => $this->belanja->count_filtered($this->data),
	                        "data" => $data,
	                );
	        echo json_encode($output);   
    }
    function check_databelanja(){
    	$this->data['cd_bel'] 	=$this->db->escape_str($this->input->post('cdbelanja',TRUE));
    	$this->data['id_kgt']	=$this->session->userdata('id_kegiatansub');
		$list = $this->belanja->get_codebelanja($this->data);
		if($list->num_rows()>0){
			$msg=false;
		}else{
			$msg = true;
		}
		echo json_encode(array('valid'=>$msg));
    }
    function save_belanja(){
    	    $this->form_validation->set_rules('cdbelanja', '', 'trim|required|exact_length[8]');
    		$this->form_validation->set_rules('ketbelanja', '', 'trim|required');
    		$this->form_validation->set_rules('jmhanggaranhd', '', 'trim');
    		if($this->form_validation->run() == TRUE){
				$this->data['id_kegiatan']   = $this->session->userdata('id_kegiatansub');
    			$this->data['cdbelanja']=$this->db->escape_str($this->input->post('cdbelanja',TRUE));
    			$this->data['ketbelanja']=$this->db->escape_str($this->input->post('ketbelanja',TRUE));
    			$this->data['jmhanggaran']=$this->db->escape_str($this->input->post('jmhanggaranhd',TRUE));
    			$this->data['id_kegiatan']=$this->session->userdata('id_kegiatansub');
    			$input = array(
							'id_kegiatan' => $this->data['id_kegiatan'],
							'code_belanja' => $this->data['cdbelanja'],
							'ket'=> $this->data['ketbelanja'],
							'prev_modal' => $this->data['jmhanggaran'] ,
							'transaksi' => '0' ,
							'last_modal' => $this->data['jmhanggaran'] ,
							'c_date' => date('Y-m-d H:i:s',now()),
							'u_date' => date('Y-m-d H:i:s',now())
						);
    			$msg    = "success";
    			$insert = $this->db->insert('sys_belanja',$input);
    			$update_prev=$this->belanja->update_kegprev($this->data); 
    			$update_kegtotsis=$this->belanja->update_kegtotsis($this->data);
				if($insert){
	                $msg    = "success";
	               $this->db->cache_delete_all();
            	}else{
            		$msg    = "error";
            	}
    		}
    		echo json_encode(array('msg'=>$msg));
    }
    function update_databelanja(){
    	    $this->form_validation->set_rules('editcdbelanja', '', 'trim|required|exact_length[8]');
    		$this->form_validation->set_rules('editketbelanja', '', 'trim|required');
    		$this->form_validation->set_rules('editjmhanggaranhd', '', 'trim');
    		if($this->form_validation->run() == TRUE){
    			$this->data['cdbelanja']=$this->db->escape_str($this->input->post('editcdbelanja',TRUE));
    			$this->data['ketbelanja']=$this->db->escape_str($this->input->post('editketbelanja',TRUE));
    			$this->data['jmhanggaran']=$this->db->escape_str($this->input->post('editjmhanggaranhd',TRUE));
    			$last_modal=$this->data['jmhanggaran']-$this->session->userdata('sessremoteprev_modal');
    			$akhirmodal=$last_modal+$this->session->userdata('sessremotelast_modal');
    			$this->data['id_kegiatan']=$this->session->userdata('id_kegiatansub');
    			$data_edit = array(
							'code_belanja' => $this->data['cdbelanja'],
							'ket'=> $this->data['ketbelanja'],
							'prev_modal' => $this->data['jmhanggaran'] ,
							'last_modal' => $akhirmodal,
							'u_date' => date('Y-m-d H:i:s',now())
						);
    			$msg    = "success";
    			$update = $this->db->where("id_belanja",$this->session->userdata('sesid_belanja'))->update('sys_belanja',$data_edit);
    			$update_prev=$this->belanja->update_kegprev($this->data);
    			$update_kegtotsis=$this->belanja->update_kegtotsis($this->data);
				if($update){
	                $msg    = "success";
	                $this->cache->clean();
	                $this->db->cache_delete_all();
            	}else{
            		$msg    = "error";
            	}
    		}
    		echo json_encode(array('msg'=>$msg));
    }
    function delete_databelanja(){
    	$id_get	=$this->db->escape_str($this->input->post('take',TRUE));
    	$load_chabeledt='is_belanja_'.$id_get;
    	$id_kgtn = explode(',',$this->dpalib->dehex($id_get));
		$this->data['id']    = $id_kgtn[0];
		$this->data['where']	= 'id_belanja';
		$this->data['id_kegiatan']=$this->session->userdata('id_kegiatansub');
		if ( !$check_data = $this->cache->get($load_chabeledt)){
			$check_data = $this->belanja->get_codebel($this->data);
			if($check_data->num_rows()>0){
				$delete=$this->db->where_in('id_belanja',$this->data['id'])->delete('sys_belanja');
				$update_prev=$this->belanja->update_kegprev($this->data);
				$update_kegtotsis=$this->belanja->update_kegtotsis($this->data);
				$this->db->cache_delete_all();
				$this->cache->delete($load_chabeledt);
				$msg=true;
			}else{
				$msg=false;
			}
		}else{
			$get_dat= $this->cache->get($load_chabeledt);
			$delete=$this->db->where_in('id_belanja',$this->data['id'])->delete('sys_belanja');
			$update_prev=$this->belanja->update_kegprev($this->data);
			$update_kegtotsis=$this->belanja->update_kegtotsis($this->data);
			$update_prev=$this->belanja->update_kegprev($this->data);
			$this->db->cache_delete_all();
			$this->cache->delete($load_chabeledt);
			$msg=true;
		}
		echo json_encode(array('msg'=>$msg));
    }
       function show_databelanja(){
    	$id_get	=$this->db->escape_str($this->input->post('take',TRUE));
    	$datestring 			= 	"%d %M %Y %h:%i:%s";
    	$id_kgtn = explode(',',$this->dpalib->dehex($id_get));
		$this->data['id']    = $id_kgtn[0];
		$this->data['remotecode_belanja']    = $id_kgtn[1];
		$this->data['where']	= 'id_belanja';
		$load_chabeledt='is_belanja_'.$id_get;
		$datestring = 'Year: %Y Month: %m Day: %d - %h:%i %a';
		 if ( !$check_data = $this->cache->get($load_chabeledt)){
		 	$check_data=$this->belanja->get_codebel($this->data);
			if ($check_data->num_rows() > 0 ){
				$msg=true;
				$check_data=$check_data->row();
				$id=$this->dpalib->enhex($check_data->id_kegiatan.','.$check_data->code_dpk);
				$id_kegiatan=$check_data->id_kegiatan;
				$code_dpk=$check_data->code_dpk;
				$code_belanja=$check_data->code_belanja;
				$ket=$check_data->ket;
				$prev_modal=$check_data->prev_modal;
				$transaksi=$check_data->transaksi;
				$last_modal=$check_data->last_modal;
				$u_date	=date("d M Y H:i:s", strtotime($check_data->u_date));
				$sess_data['sessremotecode_belanja']	=	$this->data['remotecode_belanja'];
				$sess_data['sessremoteprev_modal']	=	$prev_modal;
				$sess_data['sessremotelast_modal']	=	$last_modal;
				$sess_data['sessremoteid_kegiatan']	=	$id;
				$sess_data['sesid_belanja']	=	$this->data['id'] ;
        		$this->session->set_userdata($sess_data);
				$this->cache->save($load_chabeledt, $check_data, 300);
			}else{
				$msg=false;
				$id='';
				$id_kegiatan='';
				$code_dpk='';
				$code_belanja='';
				$ket='';
				$prev_modal='';
				$transaksi='';
				$last_modal='';
				$u_date='';
			}
		 }else{
		 	$get_data = $this->cache->get($load_chabeledt);
		 	$msg=true;
		 	$id=$this->dpalib->enhex($get_data->id_kegiatan.','.$get_data->code_dpk);
			$id_kegiatan=$get_data->id_kegiatan;
			$code_dpk=$get_data->code_dpk;
			$code_belanja=$get_data->code_belanja;
			$ket=$get_data->ket;
			$prev_modal=$get_data->prev_modal;
			$transaksi=$get_data->transaksi;
			$last_modal=$get_data->last_modal;
			$u_date	=date("d M Y H:i:s", strtotime($get_data->u_date));
			$sess_data['sessremotecode_belanja']	=	$this->data['remotecode_belanja'];
			$sess_data['sessremoteprev_modal']	=	$prev_modal;
			$sess_data['sessremotelast_modal']	=	$last_modal;
			$sess_data['sessremoteid_kegiatan']	=	$id;
			$sess_data['sesid_belanja']	=	$this->data['id'] ;	
        	$this->session->set_userdata($sess_data);
		 }
		echo json_encode(array('msg'=>$msg,'a'=>$prev_modal,'b'=>$transaksi,'c'=>$last_modal,'d'=>$u_date,'e'=>$id));
    }
    function get_beli(){
    	$id_get	=$this->db->escape_str($this->input->post('take',TRUE));
    	$id_kgtn = explode(',',$this->dpalib->dehex($id_get));
		$this->data['id']    = $id_kgtn[0];
		$this->data['remotecode_belanja']    = $id_kgtn[1];
		$this->data['where']	= 'id_belanja';
		$load_chabeledt='is_transaksi_'.$id_get;
		 if ( !$check_data = $this->cache->get($load_chabeledt)){
		 	$check_data=$this->belanja->get_codebel($this->data);
			if ($check_data->num_rows() > 0 ){
				$msg=true;
				$check_data=$check_data->row();
				$prev_modal=$check_data->prev_modal;
				$transaksi=$check_data->transaksi;
				$last_modal=$check_data->last_modal;
				$sess_data['sessremotecode_belanjatrans']	=	$this->data['id'] ;
				$sess_data['transprev_modal']	=	$prev_modal ;
				$sess_data['transtransaksi']	=	$transaksi ;
				$sess_data['translast_modal']	=	$last_modal ;
        		$this->session->set_userdata($sess_data);
				$this->cache->save($load_chabeledt, $check_data, 300);
			}else{
				$msg=false;
				$prev_modal='';
				$transaksi='';
				$last_modal='';
			}
		 }else{
		 	$get_data = $this->cache->get($load_chabeledt);
		 	$msg=true;
			$prev_modal=$get_data->prev_modal;
			$transaksi=$get_data->transaksi;
			$last_modal=$get_data->last_modal;
			$sess_data['sessremotecode_belanjatrans']	=	$this->data['id'] ;
			$sess_data['transprev_modal']	=	$prev_modal ;
			$sess_data['transtransaksi']	=	$transaksi ;
			$sess_data['translast_modal']	=	$last_modal ;
        	$this->session->set_userdata($sess_data);
		 }
		echo json_encode(array('msg'=>$msg,'a'=>$prev_modal,'b'=>$transaksi,'c'=>$last_modal));
    }
    function check_databelanjaedit(){
    	$id_get	=$this->db->escape_str($this->input->post('editcdbelanja',TRUE));
    	$this->data['id_kgt'] 	=$this->session->userdata('id_kegiatansub');
    	$id_bel=$this->session->userdata('sessremotecode_belanja');
		$id_kgt=$this->session->userdata('sessremoteid_kegiatan');
		$this->data['id']    = $id_get;
		$list = $this->belanja->get_codebelanjaedit($this->data);
		if($list->num_rows()>0){
			if($id_get == $id_bel){
				$msg = true;
			}else{
				$msg=false;	
			}
		
		}else{
			$msg = true;
		}
		echo json_encode(array('valid'=>$msg));
    }
    function get_addbeli(){
    	$this->form_validation->set_rules('buyer', '', 'trim|required');
    	$this->form_validation->set_rules('tglbeli', '', 'trim|required');
    	$this->form_validation->set_rules('jmhhgbelihd', '', 'trim|required');
    	$this->form_validation->set_rules('ketbelanjatrans', '', 'trim|required');
		if($this->form_validation->run() == TRUE){
			$this->data['buyer']   = $this->db->escape_str($this->input->post('buyer',TRUE));
			$this->data['tglbeli']=$this->db->escape_str($this->input->post('tglbeli',TRUE));
			$date = DateTime::createFromFormat('d/m/Y', $this->data['tglbeli']);
			$newDate=$date->format('Y-m-d');
			$this->data['jmhhgbelihd']=$this->db->escape_str($this->input->post('jmhhgbelihd',TRUE));
			$this->data['ketbelanjatrans']=$this->db->escape_str($this->input->post('ketbelanjatrans',TRUE));
			$jmhtotalbel=$this->data['jmhhgbelihd']+$this->session->userdata('transtransaksi');
			$last_modal=$this->session->userdata('transprev_modal')-$jmhtotalbel;
			if($jmhtotalbel>$this->session->userdata('transprev_modal')){
				$msg    = "error";
			}else{
				$this->data['id_kegiatan']=$this->session->userdata('id_kegiatansub');
				$data_edit = array(
							'last_modal' => $last_modal,
							'transaksi'=> $jmhtotalbel,
							'u_date' => date('Y-m-d H:i:s',now())
						);
    			$msg    = "success";
    			$update = $this->db->where("id_belanja",$this->session->userdata('sessremotecode_belanjatrans'))->update('sys_belanja',$data_edit);
				$input = array(
							'id_belanja' => $this->session->userdata('sessremotecode_belanjatrans'),
							'tgl_transaksi' => $newDate,
							'penerima' => $this->data['buyer'],
							'uraian'=> $this->data['ketbelanjatrans'],
							'total_transaksi' => $this->data['jmhhgbelihd'] ,
							'c_date' => date('Y-m-d H:i:s',now()),
							'u_date' => date('Y-m-d H:i:s',now())
						);
				$msg    = "success";
				$insert = $this->db->insert('sys_transaksi',$input);
				$update_kegtotsis=$this->belanja->update_kegtotsis($this->data);
				$update_prev=$this->belanja->update_kegprev($this->data);
				if($update_kegtotsis){
	                $msg    = "success";
	                $this->cache->clean();
	                $this->db->cache_delete_all();
	        	}else{
	        		$msg    = "error";
	        	}
        	}
		}
		echo json_encode(array('msg'=>$msg));
    }
}