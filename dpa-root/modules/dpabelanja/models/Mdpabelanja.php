<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdpabelanja extends CI_Model {
    var $table= 'sys_belanja';
    var $column = array('id_belanja','code_belanja','ket'
        );
    var $order = array('code_belanja' => 'asc');
	function __constuct()
	{
		parent::__constuct();  
		loader::database();    
	}
	private function _get_datatables_query($data){
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column as $item)
        {
            if($_POST['search']['value'])
                ($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
            $column[$i] = $item;
            $i++;
            $this->db->where('sys_belanja.id_kegiatan',$data['idview']);
        }
 
        if(isset($_POST['order']))
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables($data){
        $this->_get_datatables_query($data);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $this->db->join('sys_kegiatan', 'sys_belanja.id_kegiatan = sys_kegiatan.id_kegiatan');
        $this->db->where('sys_belanja.id_kegiatan',$data['idview']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered($data){
        $this->_get_datatables_query($data);
        $this->db->join('sys_kegiatan', 'sys_belanja.id_kegiatan = sys_kegiatan.id_kegiatan');
        $this->db->where('sys_belanja.id_kegiatan',$data['idview']);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all($data){	
        $this->db->join('sys_kegiatan', 'sys_belanja.id_kegiatan = sys_kegiatan.id_kegiatan');
        $this->db->where('sys_belanja.id_kegiatan',$data['idview']);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function get_codebelanja($data){
        $this->db->cache_on();
        $this->db->select('code_belanja,id_kegiatan');
        $this->db->where('code_belanja',$data['cd_bel']);
        $this->db->where('id_kegiatan',$data['id_kgt']);
        $result=$this->db->get($this->table);
        $this->db->cache_off();
        return $result;
    }
    function bel_codekgtn($data){
        $this->db->cache_on();
        $this->db->select('id_kegiatan, code_dpk, uraian_kegiatan');
        $this->db->where('id_kegiatan',$data['id_kegiatansubload']);
        $result = $this->db->get('sys_kegiatan');
        $this->db->cache_off();
        return $result;
    }
    function get_codebel($data){
        $this->db->cache_on();
        $this->db->select('sys_belanja.id_belanja,sys_belanja.code_belanja,sys_belanja.ket,sys_belanja.prev_modal,sys_belanja.transaksi,sys_belanja.last_modal,sys_belanja.u_date,sys_kegiatan.code_dpk,sys_kegiatan.id_kegiatan');
        $this->db->join('sys_kegiatan', 'sys_belanja.id_kegiatan = sys_kegiatan.id_kegiatan');
        $this->db->where($data['where'],$data['id']);
        $result=$this->db->get($this->table);
        $this->db->cache_off();
        return $result;
    }
    function get_codebelanjaedit($data){
        $this->db->cache_on();
        $this->db->select('sys_belanja.id_belanja,sys_belanja.code_belanja,sys_belanja.ket,sys_belanja.prev_modal,sys_belanja.transaksi,sys_belanja.last_modal,sys_belanja.u_date,sys_kegiatan.code_dpk,sys_kegiatan.id_kegiatan');
        $this->db->join('sys_kegiatan', 'sys_belanja.id_kegiatan = sys_kegiatan.id_kegiatan');
        $this->db->where('sys_belanja.code_belanja',$data['id']);
        $this->db->where('sys_belanja.id_kegiatan',$data['id_kgt']);
        $result=$this->db->get($this->table);
        $this->db->cache_off();
        return $result;
    }
    function update_kegtotsis($data) {
        return $this->db->query( 
        "update sys_kegiatan SK 
        JOIN (SELECT SB.id_kegiatan ,SUM(SB.transaksi) AS sum_trans, SUM(SB.last_modal) AS sum_last_modal
                FROM sys_belanja SB 
                WHERE SB.id_kegiatan=".$data['id_kegiatan']."
        )
        SJ
        ON SJ.id_kegiatan = SK.id_kegiatan
        SET SK.tot_pengeluaran = SJ.sum_trans,
            SK.sisa_anggaran = SJ.sum_last_modal
        ");
    }
    function update_kegprev($data) {
        return $this->db->query( 
        "update sys_kegiatan SK 
        JOIN (SELECT SB.id_kegiatan ,SUM(SB.prev_modal) AS sum_prev_modal
                FROM sys_belanja SB 
                WHERE SB.id_kegiatan=".$data['id_kegiatan']."
        )
        SJ
        ON SJ.id_kegiatan = SK.id_kegiatan
        SET SK.anggaran = SJ.sum_prev_modal");
    }

}