  <?php $kegiananama=$kegiatan->row();
            $kode=$kegiananama->code_dpk;
            $uraian=$kegiananama->uraian_kegiatan;
       ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$text.' Kegiatan :'?>
      </h1>
      <p><?=$kode.' - '.$uraian?>
      </p>
      <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=site_url('daftar-kegiatan')?>"><i class="fa fa-dashboard"></i> Kegiatan</a></li>
        <li class="active"><?=$text?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box box-success">
            <div class="box-header">
            <h3 class="box-title"> </h3>
              <div class="pull-right box-tools">
                <button type="button" id="addata" class="btn btn-success btn-sm" data-toggle="tooltip" title="Add Data">
                  <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="daftar_belanja" class="table dt-responsive table-striped table-bordered nowrap" width="100%" cellspacing="0">
                <thead>
            <tr>
              <th width="5%">No</th>
              <th width="13%">Kode Belanja</th>
              <th>Keterangan</th>
              <!--<th>Modal Awal</th>
              <th>Modal Akhir</th>
              <th>Total Transaksi</th>-->
              <th width="10%">Action</th>
            </tr>
                </thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->







<div class="modal fade" id="addmodal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah <?=$text?></h4>
      </div>
          <!--<div id="tampil"></div>-->
          <?php $attributes = array('role'=>'form','id'=>'modaddbelanja','class'=>'form-horizontal');
           echo form_open('',$attributes);
           ?>
          <div class="modal-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">Kode Belanja</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="cdbelanja" placeholder="Kode Belanja" maxlength="8"/>
                </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Uraian Belanja</label>
              <div class="col-sm-10">
                <textarea class="form-control" cols="2" rows="2" name="ketbelanja" placeholder="Uraian Belanja"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jumlah Anggaran Belanja</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control autonumber" data-a-sign="Rp. " name="jmhanggaran" placeholder="Rp. 0,00" />
                  <input type="hidden" class="form-control autonumber" name="jmhanggaranhd" />
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
          </div>
          <?php echo form_close(); ?>
      </div>
  </div>
</div>

<div class="modal fade" id="editmodal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit <?=$text?></h4>
      </div>
          <!--<div id="tampil"></div>-->
          <?php $attributes = array('role'=>'form','id'=>'modeditbelanja','class'=>'form-horizontal');
           echo form_open('',$attributes);
           ?>
          <div class="modal-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">Kode Belanja</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="editcdbelanja" id="editcdbelanja" placeholder="Kode Belanja" maxlength="8"/>
                </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Uraian Belanja</label>
              <div class="col-sm-10">
                <textarea class="form-control" cols="2" rows="2" name="editketbelanja" id="editketbelanja" placeholder="Uraian Belanja"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jumlah Anggaran Belanja</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control autonumber" data-a-sign="Rp. " name="editjmhanggaran" id="editjmhanggaran"/>
                  <input type="hidden" class="form-control autonumber" name="editjmhanggaranhd" />
              </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Jumlah Harga Transaksi </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control autonumber" data-a-sign="Rp. "  disabled="disabled" id="edittransaksi" name="edittransaksi" />
                  <input type="hidden" class="form-control autonumber" name="edittransaksihd" />
              </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Sisa Jumlah Anggaran </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control autonumber" data-a-sign="Rp. "  disabled="disabled" id="editlastmodal" name="editlastmodal" />
                  <input type="hidden" class="form-control autonumber" name="editlastmodalhd" />
              </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Tanggal </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control autonumber" data-a-sign="Rp. "  disabled="disabled" id="edittglkgtn" name="edittglkgtn" />
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
          </div>
          <?php echo form_close(); ?>
      </div>
  </div>
</div>

<div class="modal fade" id="transmodal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Transaksi</span></h4>
      </div>
          <!--<div id="tampil"></div>-->
          <?php $attributes = array('role'=>'form','id'=>'modtransbelanja','class'=>'form-horizontal');
           echo form_open('',$attributes);
           ?>
          <div class="modal-body">
            <div class="form-group">
                <label class="col-sm-2 control-label">Fungsi Belanja</label>
                <div class="col-sm-10 control-labelr">
                  <span class="fungsitrans"></span>
                  <input type="hidden" class="form-control" name="cdbelanjatrans" id="cdbelanjatrans" placeholder="Kode Belanja" disabled="disabled" />
                </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Tanggal </label>
              <div class="col-sm-10">
                <div class="input-group date" id="dategetin">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="datepicker" name="tglbeli" placeholder="dd/mm/yyyy">
                  </div>
                </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Uraian Belanja</label>
              <div class="col-sm-10">
                <textarea class="form-control" cols="2" rows="2" name="ketbelanjatrans" id="ketbelanjatrans" placeholder="Uraian Belanja"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Pembeli</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" cols="2" rows="2" name="buyer" id="buyer" placeholder="Buyer">
              </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Jumlah Harga Pembelian </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control autonumber" data-a-sign="Rp. "  id="jmhhgbeli" name="jmhhgbeli" placeholder="Rp. 0,00" />
                  <input type="hidden" class="form-control" name="jmhhgbelihd" id="jmhhgbelihd" />
              </div>
            </div>
            <div class="form-group">
            <label class="col-sm-2 control-label">Jumlah Harga Transaksi </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control autonumber" data-a-sign="Rp. "  id="jmhhgtrans" name="jmhhgtrans" disabled="disabled" placeholder="Rp. 0,00" />
                  <input type="hidden" class="form-control" id="jmhhgtranshd" name="jmhhgtranshd" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Jumlah Anggaran Belanja</label>
              <div class="col-sm-10">
                  <input type="text" class="form-control autonumber" data-a-sign="Rp. " name="jmhanggarantrans" id="jmhanggarantrans" disabled="disabled" placeholder="Rp. 0,00"/>
                  <input type="hidden" class="form-control" id="jmhanggarantranshd" name="jmhanggarantranshd" />
              </div>
            </div>
            
            <div class="form-group">
            <label class="col-sm-2 control-label">Sisa Jumlah Anggaran </label>
              <div class="col-sm-10">
                  <input type="text" class="form-control autonumber" data-a-sign="Rp. "  disabled="disabled" id="lastmodaltrans" name="lastmodaltrans" placeholder="Rp. 0,00"/>
                  <input type="hidden" class="form-control" id="lastmodaltranshd" name="lastmodaltranshd" />
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary waves-effect waves-light ">Save changes</button>
          </div>
          <?php echo form_close(); ?>
      </div>
  </div>
</div>
<div class="modal fade modal-flex" id="delmodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body" style="text-align: center">
          <div id="text" ></div>
          <br/>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="confdel">Ya, Hapus !</button>
          </div>
        </div>
    </div>
</div>
