<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdpalogin extends CI_Model {
    var $table= 'sys_log';
	function __constuct()
	{
		parent::__constuct();  // Call the Model constructor 
		loader::database();    // Connect to current database setting.
	}
	function getusername($data){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('sys_profile', 'sys_log.id_user = sys_profile.id_user');
		$this->db->where('sys_profile.email', $data['email']);
		$this->db->where('sys_log.pass_log', $data['password']);
		$query = $this->db->get();
		return $query;
	}
	function getemail($data){
        $this->db->join('sys_profile', 'sys_profile.id_user = sys_log.id_user');
        $this->db->where('sys_profile.email', $data['email']);
        return $this->db->get($this->table);
    }
    function valdesi($data){
        $this->db->join('sys_profile', 'sys_profile.id_user = sys_log.id_user');
        $this->db->where('sys_profile.email', $data['email']);
        $this->db->where('sys_log.key_uppass', $data['key_uppass']);
        $this->db->where('sys_log.u_date', $data['date']);
        return $this->db->get($this->table);
    }
    function valdesi_key($data){
        $this->db->where('act_key', $data['key']);
		return $this->db->get($this->table);
	}
}