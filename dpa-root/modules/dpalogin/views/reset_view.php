<div class="login-box">
  <div class="login-logo">
    <a href="<?=site_url()?>"><b>System</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Enter Your Password</p>

    <?php $attributes = array('role'=>'form','id'=>'reset_form','autocomplete'=>'off');
        	echo form_open('',$attributes); ?>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="New Password" name="npasscode">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Conf New Password" name="ncpasscode">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
            	<a href="<?=site_url('system-login')?>">Login</a><br>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="reset_form" class="btn btn-primary btn-block btn-flat">Reset</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->