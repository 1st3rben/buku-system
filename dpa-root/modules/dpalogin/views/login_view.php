<div class="login-box">
  <div class="login-logo">
    <a href="<?=site_url()?>"><b>System</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <?php $attributes = array('role'=>'form','id'=>'login_form','autocomplete'=>'off');
        	echo form_open('',$attributes); ?>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="mail">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="passcode">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
            	<a href="<?=site_url('forget-pass')?>">I forgot my password</a><br>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="login_form" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->