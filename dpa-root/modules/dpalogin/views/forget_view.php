<div class="login-box">
  <div class="login-logo">
    <a href="<?=site_url()?>"><b>System</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">To reset your password, please enter your email address</p>

    <?php $attributes = array('role'=>'form','id'=>'forget_form','autocomplete'=>'off');
        	echo form_open('',$attributes); ?>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name="mail">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="forget_form" class="btn btn-primary btn-block btn-flat">Send</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->