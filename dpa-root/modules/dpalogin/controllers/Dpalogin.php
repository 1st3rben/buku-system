<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dpalogin extends MX_Controller {
	public $data = array(
			'title'     => 'DPA',
			'text'     => 'System Login',
			'link'    => 'system-login',
			'author'    => 'Adi.Sh0x'
		);
	public function __construct(){
        parent::__construct();
    	
       //$this->load->model('dpalogin/Mdpalogin', 'login');
    }
	public function index(){
		if(!sh0x_get_in()){
			$this->_login();
       }else{
       	redirect('dashboard');
       }
			
	}
	private function _login(){
			$view='dpalogin/login_view';
			$this->dpalib->templatelogin($view,$this->data);
	}
	public function forgpass(){
		if(!sh0x_get_in()){
			$this->_forgpasss();
       }else{
       	redirect('dashboard');
       }
			
	}
	private function _forgpasss(){
			$view='dpalogin/forget_view';
			$this->dpalib->templatelogin($view,$this->data);
	}

	function auth_login(){
		if(!sh0x_get_in()){
			$this->form_validation->set_rules('mail', '', 'required');
			$this->form_validation->set_rules('passcode', '', 'required');
			$msg    = "error";
			$notif	= "";
			$hash = $this->security->get_csrf_hash();
			$salah='';
			if ($this->form_validation->run() == TRUE){
				$this->data['email']    		= $this->db->escape_str($this->input->post('mail',TRUE));
				$this->data['password']    	= MD5($this->db->escape_str($this->input->post('passcode',TRUE)));
				$query=$this->login->getusername($this->data);

				if($query->num_rows()>0){
					foreach ($query->result() as $idst) {
							$idst = $idst->status;
						if($idst == 0){
							foreach ($query->result() as $view) {
								$sess_data['superworm']		=	TRUE;
								$sess_data['wormood']		=	$view->id_user;
								$sess_data['wormname']		=	$view->nm_user;
								$sess_data['wormlevel']		=	$view->level;
								$sess_data['wormemail']		=	$view->email;
								$this->session->set_userdata($sess_data);
								$this->session->set_flashdata('notif','<script>toastr.success("Selamat datang '.$view->nm_user.'");</script>');
							}
							$msg    = "success";
						}elseif($idst == 2){
							$msg    = "info";
						}elseif($idst == 1){
							$msg    = "warning";
						}
					}
				}else{
					$msg    = "error";
				}
			}
			echo json_encode(array("msg"=>$msg, "disperindag"=>$hash));
		}else{
			redirect('dashboard');
		}
			
	}
	function auth_logout(){
    		if(!sh0x_get_in()){redirect('system-login'); } else {
			$array_items = array('superworm' 	=> FALSE, 
								  'wormood' 	=> '',
								  'wormname' 	=> '',
								  'wormlevel'	=> '');
			$this->session->unset_userdata($array_items);
			$this->session->sess_destroy();
			$this->db->cache_delete_all();
			//$this->db->cache_delete('mailworm', 'ServiceLoginAuth');
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
			$this->output->set_header('Cache-Control: no-store, no-cache,must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0,pre-check=0',false);
			$this->output->set_header('Pragma:no-cache');
			redirect('system-login');
		}
	}
	function sendath(){
		if(!sh0x_get_in()){
		$this->form_validation->set_rules('mail', '', 'required');
		$msg    = "error";
		$notif	= "";
		if ($this->form_validation->run() == TRUE){
			$this->data['email']    		= $this->db->escape_str($this->input->post('mail',TRUE));
			$query=$this->login->getemail($this->data);
			$psuc = '<script>toastr.success("Silahan buka notifikasi pada email");</script>';
			$pro   = '<script>toastr.error("Pesan Gagal dikirim");</script>';
			if($query->num_rows()>0){
				$msg    = "success";
				$judul='Reset Password';
				$tujuan=$this->data['email'] ;
				$datestring 			= 	"%Y-%m-%d %h:%i:%s";
				$time 					= 	time();
				$email 					=   $this->dpalib->safe_hexc($this->data['email']);
				$rand					= rand(0,1000);
				$time_send				= mdate($datestring, $time);
				$activation_key			= 	$this->dpalib->encode($rand.','.$time_send.','.$email);
				$time_now				=	date('Y-m-d H:i:s',time());
				$pesan= '<a href="http://localhost/disperindag-system/conf/'.$activation_key.'" target="_blank">klik </a> untuk aktivasi';
				$snma=$this->dpalib->send_email($pesan,$judul,$tujuan,$psuc,$pro);
				foreach ($query->result() as $value) {
					$data_edit = array(
							'key_uppass' => $rand,
							'u_date' => $time_send,
							'act_key' => $activation_key
						);
					$update = $this->db->where("id_user",$value->id_user)->update('sys_log',$data_edit);
				}
				$this->db->cache_delete_all();
				$this->session->set_flashdata('notif',$snma);
			}else{
				$msg    = "error";
				$snma = "email salah";
			}
		}
		echo json_encode(array("msg"=>$msg,"pesan"=>$snma));
		}else{
			redirect('dashboard');
		}
	}
	function getauth(){
		if(!sh0x_get_in()){
			$key=$this->uri->segment(2);
			$this->data['key']=$this->uri->segment(2);
			if (is_null($this->data['key']) || empty($this->data['key']) || !isset($this->data['key'])){
				$psuc = '<script>toastr.error("Ulangi Aktivasi Lagi");</script>';
				$this->session->set_flashdata('notif',$psuc);
				redirect('system-login');
			}else{
				$result_key=$this->login->valdesi_key($this->data);
				if($result_key->num_rows()>0){
					$decode=$this->dpalib->decode($key);
					$explode_data= explode(",", $decode);
					$this->data['email'] = $this->dpalib->safe_hexd($explode_data[2]);
					$this->data['key_uppass'] = $explode_data[0];
					$this->data['date'] = $explode_data[1];
					$result=$this->login->valdesi($this->data);
					if($result->num_rows()>0){
						$sess_data['email_re'] =$this->db->escape_str($this->data['email']);
      					$this->session->set_userdata($sess_data);
						$time1					=	strtotime($explode_data[1]);
						$time_now				=	date('Y-m-d H:i:s',time());
						$time2					=	strtotime($time_now);
						$sum					=	$time2-$time1;
						if($sum<=1800){
							$this->db->cache_delete_all();
							$view='dpalogin/reset_view';
							$this->dpalib->templatelogin($view, $this->data);
							$psuc = '<script>toastr.success("silahkan");</script>';
						}else{
							$psuc = '<script>toastr.error("Melewati batas waktu yang telah diberikan, silahkan ulangi prosedur dari awal !");</script>';
							$this->session->set_flashdata('notif',$psuc);
							redirect('system-login');
						}
					}else{
						$psuc = '<script>toastr.error("Key Activation telah berubah !");</script>';
						$this->session->set_flashdata('notif',$psuc);
						redirect('system-login');
					}
				}else{
					$psuc = '<script>toastr.error("Key Activation telah berubah !1");</script>';
					$this->session->set_flashdata('notif',$psuc);
					redirect('system-login');
				}
			}

		}else{
			redirect('dashboard');
		}
	}
	function forgnewpass(){
		if(!sh0x_get_in()){			
		$this->form_validation->set_rules('ncpasscode', '', 'required');
		$msg    = "error";
		$notif	= "";
		if ($this->form_validation->run() == TRUE){
			$this->data['password']    		= md5($this->db->escape_str($this->input->post('ncpasscode',TRUE)));
			$this->data['email']=$this->session->userdata('email_re');
			$query=$this->login->getemail($this->data);
			if($query->num_rows()>0){
					$msg    = "success";
					$judul='Reset Password';
					

					$datestring 			= 	"%Y-%m-%d %h:%i:%s";
					$time 					= 	time();
					$email 					=   $this->dpalib->safe_hexc($this->data['email']);
					$rand					= rand(4,1000);
					$time_send				= mdate($datestring, $time);
					$activation_key			= 	$this->dpalib->encode($rand.','.$time_send.','.$email);
					$time_now				=	date('Y-m-d H:i:s',time());
					foreach ($query->result() as $value) {
						$data_edit = array(
								'pass_log' => $this->data['password'],
								'key_uppass' => $rand,
								'u_date' => $time_send,
								'act_key' => $activation_key
							);
						$update = $this->db->where("id_user",$value->id_user)->update('sys_log',$data_edit);
					}
					//var_dump($this->data['password']);
					$this->db->cache_delete_all();
					$notif='<script>toastr.success("Silahkan Login");</script>';
					$this->session->set_flashdata('notif',$notif);
				}else{
					$msg    = "error";
					$notif = '<script>toastr.error("Ulangi Password anda");</script>';
			}
		}
		echo json_encode(array("msg"=>$msg,"pesan"=>$notif));
	}else{
			redirect('dashboard');
		}
	}
}