<?php
$route['system-login'] = 'Dpalogin/index';
$route['auth'] = 'Dpalogin/auth_login';
$route['logout'] = 'Dpalogin/auth_logout';
$route['forget-pass'] = 'Dpalogin/forgpass';
$route['reset-pass'] = 'Dpalogin/forgnewpass';
$route['send-auth'] = 'Dpalogin/sendath';
$route['conf/(:any)'] = 'Dpalogin/getauth/$1';
?>
