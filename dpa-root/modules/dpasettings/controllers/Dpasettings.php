<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dpasettings extends MX_Controller {
	public $data = array(
			'title'     => 'DPA',
			'text'     => 'Settings',
			'link'    => 'settings',
			'author'    => 'Adi.Sh0x',
			'js_system' =>	'sysset.min.js'
		);
	public function __construct(){
        parent::__construct();
    	if(!sh0x_get_in()){
			redirect('system-login');
        }
    }
	public function index(){
			$this->_settings();
	}
	private function _settings(){
			$view='dpasettings/settings_view';
			$this->dpalib->template_rt($view,$this->data);
	}
	function ajax_list() {
        $list = $this->settings->get_datatables();
        $data = array();
        $no = $_POST['start']+1;
        foreach ($list as $person) {
            $row = array();
            $row[] = $no++;
            $row[] = $person->nm_user;
            $row[] = $person->email;
            $datec  = date('d F Y', strtotime($person->c_date));
	        $row[] = $datec ;
            $id_link    = $this->dpalib->encode($person->id_user);
            if($person->id_user==2){
                 $row[] = '<button type="button" class="deldata'.$id_link.' btn disabled btn-xs btn-danger waves-effect waves-light" id="daftar_kegiatanbt" data-toggle="tooltip" title="Delete Data"><i class="fa fa-trash"></i>
                            </button>';
            }else{
            $row[] = '<button type="button" class="deldata'.$id_link.' btn btn-xs btn-danger waves-effect waves-light" id="daftar_kegiatanbt" onclick=javascript:deldat("'.$id_link.'") data-toggle="tooltip" title="Delete Data"><i class="fa fa-trash"></i>
                            </button>';
            }
            $data[] = $row;
        }
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->settings->count_all($this->data),
                        "recordsFiltered" => $this->settings->count_filtered($this->data),
                        "data" => $data,
                );
        echo json_encode($output);
    }
    function new_usercheck(){
    	$this->data['email']	=$this->db->escape_str($this->input->post('email',TRUE));
		$list = $this->settings->get_mail($this->data);
		if($list->num_rows()>0){
			$msg=false;
		}else{
			$msg = true;
		}
		echo json_encode(array('valid'=>$msg));
    }
    function new_user(){
    	    $this->form_validation->set_rules('nama', '', 'trim|required');
    		$this->form_validation->set_rules('email', '', 'trim|required');
    		$this->form_validation->set_rules('level', '', 'trim|required');
    		$this->form_validation->set_rules('password', '', 'trim|required');
    		$this->form_validation->set_rules('repassword', '', 'trim|required');
    		if($this->form_validation->run() == TRUE){
    			$this->data['nama']=$this->db->escape_str($this->input->post('nama',TRUE));
    			$this->data['email']=$this->db->escape_str($this->input->post('email',TRUE));
    			$this->data['level']=$this->db->escape_str($this->input->post('level',TRUE));
    			$this->data['password']=$this->db->escape_str($this->input->post('password',TRUE));
    			$this->data['repassword']=MD5($this->db->escape_str($this->input->post('repassword',TRUE)));
    			$input1 = array(
							'nm_user' => $this->data['nama'],
							'email'=> $this->data['email'],							
							'c_date' => date('Y-m-d H:i:s',now()),
							'u_date' => date('Y-m-d H:i:s',now())
						);
    			$insert = $this->db->insert('sys_profile',$input1);
    			$id = $this->db->insert_id();
    			$input2 = array(
    						'id_user'=>$id,
							'level' =>$this->data['level'] ,
							'pass_log' => $this->data['repassword'] ,
							'c_date' => date('Y-m-d H:i:s',now()),
							'u_date' => date('Y-m-d H:i:s',now())
						);
    			$insert2 = $this->db->insert('sys_log',$input2);
    			$msg    = "success";
				if($insert2){
	                $msg    = "success";
	                $this->db->cache_delete_all();
            	}else{
            		$msg    = "error";
            	}
    		}
    		echo json_encode(array('msg'=>$msg));
    }
    function del_user(){
    	$id_get	=$this->dpalib->decode($this->db->escape_str($this->input->post('take',TRUE)));
    	$check_data = $this->settings->get_codeuser($id_get);
		if($check_data->num_rows()>0){
			$delete=$this->db->where_in('id_user',$id_get)->delete('sys_profile');
			$this->db->cache_delete_all();
			$msg=true;
		}else{
			$msg=false;
		}
		echo json_encode(array('msg'=>$msg));
    }
    function prof(){
        $view='dpasettings/profile_view';
        $this->dpalib->template_rt($view,$this->data);
    }
    function prof_update(){
        $id_get    =$this->db->escape_str($this->input->post('nama',TRUE));
        $update=$this->db->set('nm_user', $id_get)->where('id_user', $this->session->userdata('wormood'))->update('sys_profile'); 
        if($update){
                $msg    = "success";
                $sess_data['wormname']     =   $id_get;
                $this->session->set_userdata($sess_data);
               $this->db->cache_delete_all();
            }else{
                $msg    = "error";
            }
        echo json_encode(array('msg'=>$msg));
    }
    function passcheck(){
    	$pass    =MD5($this->db->escape_str($this->input->post('cpass',TRUE)));
        $check_data=$this->settings->get_pass($pass); 
        if($check_data->num_rows()>0){
                $msg=true;
            }else{
               $msg=false;
            }
        echo json_encode(array('msg'=>$msg));
    }
    function prof_uppass(){
        $this->form_validation->set_rules('cpass', '', 'trim|required');
        $this->form_validation->set_rules('npass', '', 'trim|required');
        $this->form_validation->set_rules('ncpass', '', 'trim|required');
        if($this->form_validation->run() == TRUE){
            $this->data['cpass']=MD5($this->db->escape_str($this->input->post('cpass',TRUE)));
            $this->data['ncpass']=MD5($this->db->escape_str($this->input->post('ncpass',TRUE)));
            $check_data=$this->settings->get_pass($this->data);
            if($check_data->num_rows()>0){
                $update = array(
                                'pass_log' => $this->data['ncpass'],
                                'u_date' =>  date('Y-m-d H:i:s',now())
                            );
                $update=$this->db->where('id_user',$this->session->userdata('wormood'))->update('sys_log',$update);
                if($update){
                        $msg    = "success";
                    }else{
                        $msg    = "error";
                    }
            }else{
                $msg    = "error";
            }
        }
        echo json_encode(array('msg'=>$msg));
    }
}