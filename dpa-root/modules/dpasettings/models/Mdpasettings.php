<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdpasettings extends CI_Model {
    var $table= 'sys_profile';
    var $column = array('id_user','nm_user','email','c_date'
        );
    var $order = array('nm_user' => 'asc');
	function __constuct()
	{
		parent::__constuct();  
		loader::database();    
	}
	private function _get_datatables_query(){
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column as $item)
        {
            if($_POST['search']['value'])
                ($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
            $column[$i] = $item;
            $i++;
        }
 
        if(isset($_POST['order']))
        {
            $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all(){	
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    function get_mail($data){
        $this->db->cache_on();
        $this->db->where('email',$data['email']);
        $result=$this->db->get($this->table);
        $this->db->cache_off();
        return $result;
    }
    function get_codeuser($data){
        $this->db->where('id_user',$data);
        $result=$this->db->get($this->table);
        return $result;
    }
    function get_pass($data){
        $this->db->where('pass_log',$data['cpass']);
        $this->db->where('id_user',$this->session->userdata('wormood'));
        $result=$this->db->get('sys_log');
        return $result;
    }
}