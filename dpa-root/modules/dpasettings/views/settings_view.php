  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$text?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?=$text?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box box-success">
          <div class="box-header">
              <h3 class="box-title"> </h3>
              <div class="pull-right box-tools">
                <button type="button" id="addata" class="btn btn-success btn-sm" data-toggle="tooltip" title="Add Data">
                  <i class="fa fa-plus"></i></button>
              </div> 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
          <table id="data_settings" class="table dt-responsive table-striped table-bordered nowrap" width="100%" cellspacing="0">
            <thead>
            <tr>
              <th width="5%">No</th>
              <th width="15%">Nama User</th>
               <th width="15%">Email</th>
              <th>Create Date</th>
              <th width="10%">Action</th>
            </tr>
            </thead>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<div class="modal fade" id="addmodal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Tambah <?=$text?></h4>
      </div>
       <?php $attributes = array('role'=>'form','id'=>'modadduser','class'=>'form-horizontal');
       echo form_open('',$attributes);
       ?>
      <div class="modal-body">
          <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="nama" placeholder="xxxxxxxx" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" name="email" placeholder="xxxxx@xxx.xx" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Level</label>
            <div class="col-sm-10">
              <select name="level" class="form-control fclass">
                <option value="1">User</option>
                <option value="0">Admin</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" name="password" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Re-Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" name="repassword" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" />
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      <?php echo form_close(); ?>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<div class="modal fade modal-flex" id="deltrans" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body" style="text-align: center">
          <div id="text" ></div>
          <br/>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger" id="confdel">Ya, Hapus !</button>
          </div>
        </div>
    </div>
</div>