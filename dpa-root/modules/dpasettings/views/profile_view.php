  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$text?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?=$text?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
            <?php $is_profile=$this->session->userdata('wormlevel');
                 $level='user'; 
                  $img='users.png';
                  if($is_profile==0){
                    $level='Admin';
                    $img='icon_user.png';
                  }?>
                <div class="col-lg-12 col-sm-12">
                <div class="card hovercard">
                    <div class="card-background">
                        <img class="card-bkimg" alt="" src="<?=base_url()?>assets/dist/img/boxed-bg.png">
                        <!-- http://lorempixel.com/850/280/people/9/ -->
                    </div>
                    <div class="useravatar">
                        <img alt="" src="<?=base_url()?>assets/dist/img/<?=$img?>">
                    </div>
                    <div class="card-info"> <span class="card-title"><?=$level?></span>

                    </div>
                </div>
                <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <button type="button" id="stars" class="btn btn-primary" href="#tab1" data-toggle="tab">
                            <div class="hidden-xs">Profile</div>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" id="following" class="btn btn-default" href="#tab3" data-toggle="tab">
                            <div class="hidden-xs">Password</div>
                        </button>
                    </div>
                </div>

                    <div class="well" style="background-color: #fff !important">
                  <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab1">
                      <?php $attributes = array('role'=>'form','id'=>'editprof','class'=>'form-horizontal');
                       echo form_open('',$attributes);
                       ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-5">
                              <input type="text" class="form-control" name="nama" placeholder="xxxxx" value="<?=$this->session->userdata('wormname')?>" />
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Email</label>
                          <div class="col-sm-5 control-labelr">
                            <span class="emailnya"><?=$this->session->userdata('wormemail')?></span>
                          </div>
                        </div>
                        <div class="form-group" style="margin-top: 15px;">
                          <div class="col-xs-5 col-xs-offset-5">
                              <button type="submit" class="btn btn-primary">Update</button>
                          </div>
                        </div>
                      <?php echo form_close(); ?>
                    </div>
                    <div class="tab-pane fade in" id="tab3">
                      <?php $attributes = array('role'=>'form','id'=>'editpass','class'=>'form-horizontal');
                       echo form_open('',$attributes);
                       ?>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Current Password</label>
                            <div class="col-sm-5">
                              <input type="password" class="form-control" name="cpass" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">New Password</label>
                            <div class="col-sm-5">
                              <input type="password" class="form-control" name="npass" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Confrim Password</label>
                            <div class="col-sm-5">
                              <input type="password" class="form-control" name="ncpass" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" />
                            </div>
                        </div>
                        
                        <div class="form-group" style="margin-top: 15px;">
                          <div class="col-xs-5 col-xs-offset-5">
                              <button type="submit" class="btn btn-primary">Update</button>
                          </div>
                        </div>
                      <?php echo form_close(); ?>
                    </div>
                  </div>
                </div>              
                </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->