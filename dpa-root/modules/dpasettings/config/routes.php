<?php
$route['settings'] = 'Dpasettings/index';
$route['settings-view'] = 'Dpasettings/ajax_list';
$route['profile'] = 'Dpasettings/prof';
$route['settings-checkpass'] = 'Dpasettings/passcheck';
$route['profile-update'] = 'Dpasettings/prof_update';
$route['profile-update-password'] = 'Dpasettings/prof_uppass';
$route['add-user'] = 'Dpasettings/new_user';
$route['delete-user'] = 'Dpasettings/del_user';
$route['check-user'] = 'Dpasettings/new_usercheck';
?>
