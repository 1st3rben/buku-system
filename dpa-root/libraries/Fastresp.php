<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fastresp {
  protected $libcac;

  public function __construct(){
    $this->libcac =& get_instance();
    $this->initiate_cache();
  }

  public function initiate_cache(){
    $this->libcac->load->driver('cache',array('adapter' => 'apc', 'backup' => 'file', 'key_prefix' => 'nofacenoname_'));
  }
}
?>